package main

import (
	"gitlab.com/laboratorios-programacion/giantronic-api/api"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/env"
)

func main() {
	c := env.NewConfiguration()
	api.Start(c.App.Port, c.App.ServiceName, c.App.LoggerHttp, c.App.AllowedDomains)
}
