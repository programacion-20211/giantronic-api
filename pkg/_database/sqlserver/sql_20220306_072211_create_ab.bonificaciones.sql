
-- +migrate Up
CREATE TABLE ab.bonificaciones(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [id_actividad] [INT]  NOT NULL,
    [bonificacion] [INT]  NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.bonificaciones;
