
-- +migrate Up
CREATE TABLE ab.usuarios(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [email] [VARCHAR] (150) NOT NULL,
    [name] [VARCHAR] (100) NOT NULL,
    [lastname] [VARCHAR] (150) NOT NULL,
    [id_type] [INT]  ,
    [id_number] [VARCHAR] (10) ,
    [password] [VARCHAR] (150) NOT NULL,
    [cellphone] [VARCHAR] (13) ,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.usuarios;
