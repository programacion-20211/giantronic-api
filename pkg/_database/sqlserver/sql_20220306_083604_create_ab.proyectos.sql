
-- +migrate Up
CREATE TABLE ab.proyectos(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [id_actividad] [INT]  NOT NULL,
    [valor_estandar] [INT]  NOT NULL,
    [valor_critico] [INT]  NOT NULL,
    [nro_actividades_cronograma] [INT]  NOT NULL,
    [nro_actividades_ejecutadas] [INT]  NOT NULL,
    [procentaje_cumplimiento] [INT]  NOT NULL,
    [puntaje_c] [INT]  NOT NULL,
    [puntaje_d] [INT]  NOT NULL,
    [procentaje_obtenido] [INT]  NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.proyectos;
