
-- +migrate Up
CREATE TABLE ab.empleados(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [tipo_identificacion] [INT]  NOT NULL,
    [numero_identificacion] [VARCHAR] (20) NOT NULL,
    [nombre] [VARCHAR] (100) NOT NULL,
    [id_cargo] [INT]  NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.empleados;
