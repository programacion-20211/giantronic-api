
-- +migrate Up
CREATE TABLE ab.productivity_indicators(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [tipo_puntaje] [INT]  ,
    [puntaje] [INT]  ,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.productivity_indicators;
