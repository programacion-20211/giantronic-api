
-- +migrate Up
CREATE TABLE ab.messages(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [spa] [VARCHAR] (100) NOT NULL,
    [eng] [VARCHAR] (100) NOT NULL,
    [type_message] [INT]  NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.messages;
