
-- +migrate Up
CREATE TABLE ab.periodos(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [mes] [INT]  NOT NULL,
    [año] [INT]  NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.periodos;
