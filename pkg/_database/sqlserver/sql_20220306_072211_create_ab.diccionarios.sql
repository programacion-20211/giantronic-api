
-- +migrate Up
CREATE TABLE ab.diccionarios(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [name] [VARCHAR] (50) NOT NULL,
    [value] [VARCHAR] (100) NOT NULL,
    [description] [VARCHAR] (200) NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.diccionarios;
