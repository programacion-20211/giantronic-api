
-- +migrate Up
CREATE TABLE ab.roles(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [name] [VARCHAR] (50) NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.roles;
