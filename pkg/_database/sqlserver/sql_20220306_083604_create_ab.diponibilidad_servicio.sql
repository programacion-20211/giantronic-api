
-- +migrate Up
CREATE TABLE ab.diponibilidad_servicio(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [periodo_id] [INT]  NOT NULL,
    [valor_estandar] [INT]  NOT NULL,
    [valor_critico] [INT]  NOT NULL,
    [horas_totales] [INT]  NOT NULL,
    [horas_fallo] [INT]  NOT NULL,
    [metas_cumplidas] [INT]  NOT NULL,
    [puntaje_c] [INT]  NOT NULL,
    [puntaje_d] [INT]  NOT NULL,
    [puntos] [INT]  NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.diponibilidad_servicio;
