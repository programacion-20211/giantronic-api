
-- +migrate Up
CREATE TABLE ab.cargos(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [cargo] [INT]  NOT NULL,
    [porcetaje_cumplimiento_actividades] [INT]  NOT NULL,
    [porcetaje_mesa_ayuda] [INT]  NOT NULL,
    [porcetaje_disponibilidad_servicio] [INT]  NOT NULL,
    [porcetaje_niveles_servicio] [INT]  NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.cargos;
