
-- +migrate Up
CREATE TABLE ab.evaluacion_actividades(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [id_empleado] [INT]  NOT NULL,
    [periodo_id] [INT]  NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.evaluacion_actividades;
