
-- +migrate Up
CREATE TABLE ab.usuarios_roles(
    [id] INT IDENTITY(1,1) PRIMARY KEY,
    [role_id] [INT]  NOT NULL,
    [user_id] [INT]  NOT NULL,
    created_at [datetime] NOT NULL  DEFAULT (getdate()),
    updated_at [datetime] NOT NULL  DEFAULT (getdate())
);

-- +migrate Down
DROP TABLE ab.usuarios_roles;
