package bonificaciones

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesBonificacionRepository interface {
	create(m *Bonificacion) error
	update(m *Bonificacion) error
	delete(id int) error
	getByID(id int) (*Bonificacion, error)
	getAll() ([]*Bonificacion, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesBonificacionRepository {
	var s ServicesBonificacionRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newBonificacionSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
