package bonificaciones

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// Bonificacion  Model struct Bonificacion
type Bonificacion struct {
	ID           int       `json:"id" db:"id" valid:"-"`
	IdActividad  int       `json:"id_actividad" db:"id_actividad" valid:"required"`
	Bonificacion int       `json:"bonificacion" db:"bonificacion" valid:"required"`
	CreatedAt    time.Time `json:"created_at" db:"created_at"`
	UpdatedAt    time.Time `json:"updated_at" db:"updated_at"`
}

func NewBonificacion(id int, idActividad int, bonificacion int) *Bonificacion {
	return &Bonificacion{
		ID:           id,
		IdActividad:  idActividad,
		Bonificacion: bonificacion,
	}
}

func NewCreateBonificacion(idActividad int, bonificacion int) *Bonificacion {
	return &Bonificacion{
		IdActividad:  idActividad,
		Bonificacion: bonificacion,
	}
}

func (m *Bonificacion) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
