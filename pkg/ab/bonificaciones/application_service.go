package bonificaciones

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerBonificacion interface {
	CreateBonificacion(idActividad int, bonificacion int) (*Bonificacion, int, error)
	UpdateBonificacion(id int, idActividad int, bonificacion int) (*Bonificacion, int, error)
	DeleteBonificacion(id int) (int, error)
	GetBonificacionByID(id int) (*Bonificacion, int, error)
	GetAllBonificacion() ([]*Bonificacion, error)
}

type service struct {
	repository ServicesBonificacionRepository
	user       *models.User
	txID       string
}

func NewBonificacionService(repository ServicesBonificacionRepository, user *models.User, TxID string) PortsServerBonificacion {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateBonificacion(idActividad int, bonificacion int) (*Bonificacion, int, error) {
	m := NewCreateBonificacion(idActividad, bonificacion)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create Bonificacion :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateBonificacion(id int, idActividad int, bonificacion int) (*Bonificacion, int, error) {
	m := NewBonificacion(id, idActividad, bonificacion)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update Bonificacion :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteBonificacion(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetBonificacionByID(id int) (*Bonificacion, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllBonificacion() ([]*Bonificacion, error) {
	return s.repository.getAll()
}
