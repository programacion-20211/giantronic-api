package mesa_ayuda

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

// sqlServer estructura de conexión a la BD de mssql
type sqlserver struct {
	DB   *sqlx.DB
	user *models.User
	TxID string
}

func newMesaAyudaSqlServerRepository(db *sqlx.DB, user *models.User, txID string) *sqlserver {
	return &sqlserver{
		DB:   db,
		user: user,
		TxID: txID,
	}
}

// Create registra en la BD
func (s *sqlserver) create(m *MesaAyuda) error {
	var id int
	date := time.Now()
	m.UpdatedAt = date
	m.CreatedAt = date
	const sqlInsert = `INSERT INTO ab.mesa_ayuda (id_actividad, valor_estandar, valor_critico, nro_casos_asignados, nro_casos_resueltos, procentaje_cumplimiento_metas, puntaje_c, puntaje_d, procentaje_obtenido, created_at, updated_at) VALUES (@id_actividad, @valor_estandar, @valor_critico, @nro_casos_asignados, @nro_casos_resueltos, @procentaje_cumplimiento_metas, @puntaje_c, @puntaje_d, @procentaje_obtenido, @created_at, @updated_at) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	stmt, err := s.DB.Prepare(sqlInsert)
	if err != nil {
		return err
	}
	defer stmt.Close()
	err = stmt.QueryRow(
		sql.Named("id_actividad", m.IdActividad),
		sql.Named("valor_estandar", m.ValorEstandar),
		sql.Named("valor_critico", m.ValorCritico),
		sql.Named("nro_casos_asignados", m.NroCasosAsignados),
		sql.Named("nro_casos_resueltos", m.NroCasosResueltos),
		sql.Named("procentaje_cumplimiento_metas", m.ProcentajeCumplimientoMetas),
		sql.Named("puntaje_c", m.PuntajeC),
		sql.Named("puntaje_d", m.PuntajeD),
		sql.Named("procentaje_obtenido", m.ProcentajeObtenido),
		sql.Named("created_at", m.CreatedAt),
		sql.Named("updated_at", m.UpdatedAt),
	).Scan(&id)
	if err != nil {
		return err
	}
	m.ID = int(id)
	return nil
}

// Update actualiza un registro en la BD
func (s *sqlserver) update(m *MesaAyuda) error {
	date := time.Now()
	m.UpdatedAt = date
	const sqlUpdate = `UPDATE ab.mesa_ayuda SET id_actividad = :id_actividad, valor_estandar = :valor_estandar, valor_critico = :valor_critico, nro_casos_asignados = :nro_casos_asignados, nro_casos_resueltos = :nro_casos_resueltos, procentaje_cumplimiento_metas = :procentaje_cumplimiento_metas, puntaje_c = :puntaje_c, puntaje_d = :puntaje_d, procentaje_obtenido = :procentaje_obtenido, updated_at = :updated_at WHERE id = :id `
	rs, err := s.DB.NamedExec(sqlUpdate, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// Delete elimina un registro de la BD
func (s *sqlserver) delete(id int) error {
	const sqlDelete = `DELETE FROM ab.mesa_ayuda WHERE id = :id `
	m := MesaAyuda{ID: id}
	rs, err := s.DB.NamedExec(sqlDelete, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// GetByID consulta un registro por su ID
func (s *sqlserver) getByID(id int) (*MesaAyuda, error) {
	const sqlGetByID = `SELECT convert(nvarchar(50), id) id , id_actividad, valor_estandar, valor_critico, nro_casos_asignados, nro_casos_resueltos, procentaje_cumplimiento_metas, puntaje_c, puntaje_d, procentaje_obtenido, created_at, updated_at FROM ab.mesa_ayuda  WITH (NOLOCK)  WHERE id = @id `
	mdl := MesaAyuda{}
	err := s.DB.Get(&mdl, sqlGetByID, sql.Named("id", id))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return &mdl, err
	}
	return &mdl, nil
}

// GetAll consulta todos los registros de la BD
func (s *sqlserver) getAll() ([]*MesaAyuda, error) {
	var ms []*MesaAyuda
	const sqlGetAll = `SELECT convert(nvarchar(50), id) id , id_actividad, valor_estandar, valor_critico, nro_casos_asignados, nro_casos_resueltos, procentaje_cumplimiento_metas, puntaje_c, puntaje_d, procentaje_obtenido, created_at, updated_at FROM ab.mesa_ayuda  WITH (NOLOCK) `

	err := s.DB.Select(&ms, sqlGetAll)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return ms, err
	}
	return ms, nil
}
