package mesa_ayuda

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// MesaAyuda  Model struct MesaAyuda
type MesaAyuda struct {
	ID                          int       `json:"id" db:"id" valid:"-"`
	IdActividad                 int       `json:"id_actividad" db:"id_actividad" valid:"required"`
	ValorEstandar               int       `json:"valor_estandar" db:"valor_estandar" valid:"required"`
	ValorCritico                int       `json:"valor_critico" db:"valor_critico" valid:"required"`
	NroCasosAsignados           int       `json:"nro_casos_asignados" db:"nro_casos_asignados" valid:"required"`
	NroCasosResueltos           int       `json:"nro_casos_resueltos" db:"nro_casos_resueltos" valid:"required"`
	ProcentajeCumplimientoMetas int       `json:"procentaje_cumplimiento_metas" db:"procentaje_cumplimiento_metas" valid:"required"`
	PuntajeC                    int       `json:"puntaje_c" db:"puntaje_c" valid:"required"`
	PuntajeD                    int       `json:"puntaje_d" db:"puntaje_d" valid:"required"`
	ProcentajeObtenido          int       `json:"procentaje_obtenido" db:"procentaje_obtenido" valid:"required"`
	CreatedAt                   time.Time `json:"created_at" db:"created_at"`
	UpdatedAt                   time.Time `json:"updated_at" db:"updated_at"`
}

func NewMesaAyuda(id int, idActividad int, valorEstandar int, valorCritico int, nroCasosAsignados int, nroCasosResueltos int, procentajeCumplimientoMetas int, puntajeC int, puntajeD int, procentajeObtenido int) *MesaAyuda {
	return &MesaAyuda{
		ID:                          id,
		IdActividad:                 idActividad,
		ValorEstandar:               valorEstandar,
		ValorCritico:                valorCritico,
		NroCasosAsignados:           nroCasosAsignados,
		NroCasosResueltos:           nroCasosResueltos,
		ProcentajeCumplimientoMetas: procentajeCumplimientoMetas,
		PuntajeC:                    puntajeC,
		PuntajeD:                    puntajeD,
		ProcentajeObtenido:          procentajeObtenido,
	}
}

func NewCreateMesaAyuda(idActividad int, valorEstandar int, valorCritico int, nroCasosAsignados int, nroCasosResueltos int, procentajeCumplimientoMetas int, puntajeC int, puntajeD int, procentajeObtenido int) *MesaAyuda {
	return &MesaAyuda{
		IdActividad:                 idActividad,
		ValorEstandar:               valorEstandar,
		ValorCritico:                valorCritico,
		NroCasosAsignados:           nroCasosAsignados,
		NroCasosResueltos:           nroCasosResueltos,
		ProcentajeCumplimientoMetas: procentajeCumplimientoMetas,
		PuntajeC:                    puntajeC,
		PuntajeD:                    puntajeD,
		ProcentajeObtenido:          procentajeObtenido,
	}
}

func (m *MesaAyuda) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
