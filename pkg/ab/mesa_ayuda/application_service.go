package mesa_ayuda

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerMesaAyuda interface {
	CreateMesaAyuda(idActividad int, valorEstandar int, valorCritico int, nroCasosAsignados int, nroCasosResueltos int, procentajeCumplimientoMetas int, puntajeC int, puntajeD int, procentajeObtenido int) (*MesaAyuda, int, error)
	UpdateMesaAyuda(id int, idActividad int, valorEstandar int, valorCritico int, nroCasosAsignados int, nroCasosResueltos int, procentajeCumplimientoMetas int, puntajeC int, puntajeD int, procentajeObtenido int) (*MesaAyuda, int, error)
	DeleteMesaAyuda(id int) (int, error)
	GetMesaAyudaByID(id int) (*MesaAyuda, int, error)
	GetAllMesaAyuda() ([]*MesaAyuda, error)
}

type service struct {
	repository ServicesMesaAyudaRepository
	user       *models.User
	txID       string
}

func NewMesaAyudaService(repository ServicesMesaAyudaRepository, user *models.User, TxID string) PortsServerMesaAyuda {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateMesaAyuda(idActividad int, valorEstandar int, valorCritico int, nroCasosAsignados int, nroCasosResueltos int, procentajeCumplimientoMetas int, puntajeC int, puntajeD int, procentajeObtenido int) (*MesaAyuda, int, error) {
	m := NewCreateMesaAyuda(idActividad, valorEstandar, valorCritico, nroCasosAsignados, nroCasosResueltos, procentajeCumplimientoMetas, puntajeC, puntajeD, procentajeObtenido)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create MesaAyuda :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateMesaAyuda(id int, idActividad int, valorEstandar int, valorCritico int, nroCasosAsignados int, nroCasosResueltos int, procentajeCumplimientoMetas int, puntajeC int, puntajeD int, procentajeObtenido int) (*MesaAyuda, int, error) {
	m := NewMesaAyuda(id, idActividad, valorEstandar, valorCritico, nroCasosAsignados, nroCasosResueltos, procentajeCumplimientoMetas, puntajeC, puntajeD, procentajeObtenido)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update MesaAyuda :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteMesaAyuda(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetMesaAyudaByID(id int) (*MesaAyuda, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllMesaAyuda() ([]*MesaAyuda, error) {
	return s.repository.getAll()
}
