package mesa_ayuda

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesMesaAyudaRepository interface {
	create(m *MesaAyuda) error
	update(m *MesaAyuda) error
	delete(id int) error
	getByID(id int) (*MesaAyuda, error)
	getAll() ([]*MesaAyuda, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesMesaAyudaRepository {
	var s ServicesMesaAyudaRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newMesaAyudaSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
