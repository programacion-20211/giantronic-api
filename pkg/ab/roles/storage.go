package roles

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	Postgresql = "postgres"
	SqlServer  = "sqlserver"
	Oracle     = "oci8"
)

type ServicesRolRepository interface {
	create(m *Rol) error
	update(m *Rol) error
	delete(id int) error
	getByID(id int) (*Rol, error)
	getAll() ([]*Rol, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesRolRepository {
	var s ServicesRolRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newRolSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
