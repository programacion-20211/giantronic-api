package empleados

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerEmpleado interface {
	CreateEmpleado(tipoIdentificacion int, numeroIdentificacion string, nombre string, idCargo int) (*Empleado, int, error)
	UpdateEmpleado(id int, tipoIdentificacion int, numeroIdentificacion string, nombre string, idCargo int) (*Empleado, int, error)
	DeleteEmpleado(id int) (int, error)
	GetEmpleadoByID(id int) (*Empleado, int, error)
	GetAllEmpleado() ([]*Empleado, error)
}

type service struct {
	repository ServicesEmpleadoRepository
	user       *models.User
	txID       string
}

func NewEmpleadoService(repository ServicesEmpleadoRepository, user *models.User, TxID string) PortsServerEmpleado {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateEmpleado(tipoIdentificacion int, numeroIdentificacion string, nombre string, idCargo int) (*Empleado, int, error) {
	m := NewCreateEmpleado(tipoIdentificacion, numeroIdentificacion, nombre, idCargo)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create Empleado :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateEmpleado(id int, tipoIdentificacion int, numeroIdentificacion string, nombre string, idCargo int) (*Empleado, int, error) {
	m := NewEmpleado(id, tipoIdentificacion, numeroIdentificacion, nombre, idCargo)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update Empleado :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteEmpleado(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetEmpleadoByID(id int) (*Empleado, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllEmpleado() ([]*Empleado, error) {
	return s.repository.getAll()
}
