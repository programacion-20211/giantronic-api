package empleados

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// Empleado  Model struct Empleado
type Empleado struct {
	ID                   int       `json:"id" db:"id" valid:"-"`
	TipoIdentificacion   int       `json:"tipo_identificacion" db:"tipo_identificacion" valid:"required"`
	NumeroIdentificacion string    `json:"numero_identificacion" db:"numero_identificacion" valid:"required"`
	Nombre               string    `json:"nombre" db:"nombre" valid:"required"`
	IdCargo              int       `json:"id_cargo" db:"id_cargo" valid:"required"`
	Cargo                string    `json:"cargo" db:"cargo" valid:"-"`
	CreatedAt            time.Time `json:"created_at" db:"created_at"`
	UpdatedAt            time.Time `json:"updated_at" db:"updated_at"`
}

func NewEmpleado(id int, tipoIdentificacion int, numeroIdentificacion string, nombre string, idCargo int) *Empleado {
	return &Empleado{
		ID:                   id,
		TipoIdentificacion:   tipoIdentificacion,
		NumeroIdentificacion: numeroIdentificacion,
		Nombre:               nombre,
		IdCargo:              idCargo,
	}
}

func NewCreateEmpleado(tipoIdentificacion int, numeroIdentificacion string, nombre string, idCargo int) *Empleado {
	return &Empleado{
		TipoIdentificacion:   tipoIdentificacion,
		NumeroIdentificacion: numeroIdentificacion,
		Nombre:               nombre,
		IdCargo:              idCargo,
	}
}

func (m *Empleado) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
