package empleados

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

// sqlServer estructura de conexión a la BD de mssql
type sqlserver struct {
	DB   *sqlx.DB
	user *models.User
	TxID string
}

func newEmpleadoSqlServerRepository(db *sqlx.DB, user *models.User, txID string) *sqlserver {
	return &sqlserver{
		DB:   db,
		user: user,
		TxID: txID,
	}
}

// Create registra en la BD
func (s *sqlserver) create(m *Empleado) error {
	var id int
	date := time.Now()
	m.UpdatedAt = date
	m.CreatedAt = date
	const sqlInsert = `INSERT INTO ab.empleados (tipo_identificacion, numero_identificacion, nombre, id_cargo, created_at, updated_at) VALUES (@tipo_identificacion, @numero_identificacion, @nombre, @id_cargo, @created_at, @updated_at) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	stmt, err := s.DB.Prepare(sqlInsert)
	if err != nil {
		return err
	}
	defer stmt.Close()
	err = stmt.QueryRow(
		sql.Named("tipo_identificacion", m.TipoIdentificacion),
		sql.Named("numero_identificacion", m.NumeroIdentificacion),
		sql.Named("nombre", m.Nombre),
		sql.Named("id_cargo", m.IdCargo),
		sql.Named("created_at", m.CreatedAt),
		sql.Named("updated_at", m.UpdatedAt),
	).Scan(&id)
	if err != nil {
		return err
	}
	m.ID = int(id)
	return nil
}

// Update actualiza un registro en la BD
func (s *sqlserver) update(m *Empleado) error {
	date := time.Now()
	m.UpdatedAt = date
	const sqlUpdate = `UPDATE ab.empleados SET tipo_identificacion = :tipo_identificacion, numero_identificacion = :numero_identificacion, nombre = :nombre, id_cargo = :id_cargo, updated_at = :updated_at WHERE id = :id `
	rs, err := s.DB.NamedExec(sqlUpdate, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// Delete elimina un registro de la BD
func (s *sqlserver) delete(id int) error {
	const sqlDelete = `DELETE FROM ab.empleados WHERE id = :id `
	m := Empleado{ID: id}
	rs, err := s.DB.NamedExec(sqlDelete, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// GetByID consulta un registro por su ID
func (s *sqlserver) getByID(id int) (*Empleado, error) {
	const sqlGetByID = `SELECT e.id , e.tipo_identificacion, e.numero_identificacion, e.nombre, e.id_cargo, c.cargo, e.created_at, e.updated_at FROM ab.empleados e
  				JOIN ab.cargos c on c.id = e.id_cargo  WHERE id = @id `
	mdl := Empleado{}
	err := s.DB.Get(&mdl, sqlGetByID, sql.Named("id", id))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return &mdl, err
	}
	return &mdl, nil
}

// GetAll consulta todos los registros de la BD
func (s *sqlserver) getAll() ([]*Empleado, error) {
	var ms []*Empleado
	const sqlGetAll = `SELECT e.id , e.tipo_identificacion, e.numero_identificacion, e.nombre, e.id_cargo, c.cargo, e.created_at, e.updated_at FROM ab.empleados e
  				JOIN ab.cargos c on c.id = e.id_cargo `

	err := s.DB.Select(&ms, sqlGetAll)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return ms, err
	}
	return ms, nil
}
