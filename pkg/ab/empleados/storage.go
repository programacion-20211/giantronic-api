package empleados

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	Postgresql = "postgres"
	SqlServer  = "sqlserver"
	Oracle     = "oci8"
)

type ServicesEmpleadoRepository interface {
	create(m *Empleado) error
	update(m *Empleado) error
	delete(id int) error
	getByID(id int) (*Empleado, error)
	getAll() ([]*Empleado, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesEmpleadoRepository {
	var s ServicesEmpleadoRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newEmpleadoSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
