package usuarios_roles

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	Postgresql = "postgres"
	SqlServer  = "sqlserver"
	Oracle     = "oci8"
)

type ServicesUsuarioRolRepository interface {
	create(m *UsuarioRol) error
	update(m *UsuarioRol) error
	delete(id int) error
	getByID(id int) (*UsuarioRol, error)
	getAll() ([]*UsuarioRol, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesUsuarioRolRepository {
	var s ServicesUsuarioRolRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newUsuarioRolSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
