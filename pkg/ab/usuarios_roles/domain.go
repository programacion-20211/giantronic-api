package usuarios_roles

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// UsuarioRol  Model struct UsuarioRol
type UsuarioRol struct {
	ID        int       `json:"id" db:"id" valid:"-"`
	RoleId    int       `json:"role_id" db:"role_id" valid:"required"`
	UserId    int       `json:"user_id" db:"user_id" valid:"required"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
}

func NewUsuarioRol(id int, roleId int, userId int) *UsuarioRol {
	return &UsuarioRol{
		ID:     id,
		RoleId: roleId,
		UserId: userId,
	}
}

func NewCreateUsuarioRol(roleId int, userId int) *UsuarioRol {
	return &UsuarioRol{
		RoleId: roleId,
		UserId: userId,
	}
}

func (m *UsuarioRol) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
