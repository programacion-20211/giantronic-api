package usuarios_roles

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerUsuarioRol interface {
	CreateUsuarioRol(roleId int, userId int) (*UsuarioRol, int, error)
	UpdateUsuarioRol(id int, roleId int, userId int) (*UsuarioRol, int, error)
	DeleteUsuarioRol(id int) (int, error)
	GetUsuarioRolByID(id int) (*UsuarioRol, int, error)
	GetAllUsuarioRol() ([]*UsuarioRol, error)
}

type service struct {
	repository ServicesUsuarioRolRepository
	user       *models.User
	txID       string
}

func NewUsuarioRolService(repository ServicesUsuarioRolRepository, user *models.User, TxID string) PortsServerUsuarioRol {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateUsuarioRol(roleId int, userId int) (*UsuarioRol, int, error) {
	m := NewCreateUsuarioRol(roleId, userId)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create UsuarioRol :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateUsuarioRol(id int, roleId int, userId int) (*UsuarioRol, int, error) {
	m := NewUsuarioRol(id, roleId, userId)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update UsuarioRol :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteUsuarioRol(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetUsuarioRolByID(id int) (*UsuarioRol, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllUsuarioRol() ([]*UsuarioRol, error) {
	return s.repository.getAll()
}
