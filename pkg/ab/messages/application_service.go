package messages

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerMessage interface {
	CreateMessage(spa string, eng string, typeMessage int) (*Message, int, error)
	UpdateMessage(id int, spa string, eng string, typeMessage int) (*Message, int, error)
	DeleteMessage(id int) (int, error)
	GetMessageByID(id int) (*Message, int, error)
	GetAllMessage() ([]*Message, error)
}

type service struct {
	repository ServicesMessageRepository
	user       *models.User
	txID       string
}

func NewMessageService(repository ServicesMessageRepository, user *models.User, TxID string) PortsServerMessage {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateMessage(spa string, eng string, typeMessage int) (*Message, int, error) {
	m := NewCreateMessage(spa, eng, typeMessage)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create Message :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateMessage(id int, spa string, eng string, typeMessage int) (*Message, int, error) {
	m := NewMessage(id, spa, eng, typeMessage)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update Message :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteMessage(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetMessageByID(id int) (*Message, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllMessage() ([]*Message, error) {
	return s.repository.getAll()
}
