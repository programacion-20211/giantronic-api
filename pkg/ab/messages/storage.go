package messages

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesMessageRepository interface {
	create(m *Message) error
	update(m *Message) error
	delete(id int) error
	getByID(id int) (*Message, error)
	getAll() ([]*Message, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesMessageRepository {
	var s ServicesMessageRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newMessageSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
