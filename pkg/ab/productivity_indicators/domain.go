package productivity_indicators

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// ProductivityIndicator  Model struct ProductivityIndicator
type ProductivityIndicator struct {
	ID          int       `json:"id" db:"id" valid:"-"`
	TipoPuntaje string    `json:"tipo_puntaje" db:"tipo_puntaje" valid:"required"`
	Puntaje     int       `json:"puntaje" db:"puntaje" valid:"required"`
	CreatedAt   time.Time `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
}

func NewProductivityIndicator(id int, tipoPuntaje string, puntaje int) *ProductivityIndicator {
	return &ProductivityIndicator{
		ID:          id,
		TipoPuntaje: tipoPuntaje,
		Puntaje:     puntaje,
	}
}

func NewCreateProductivityIndicator(tipoPuntaje string, puntaje int) *ProductivityIndicator {
	return &ProductivityIndicator{
		TipoPuntaje: tipoPuntaje,
		Puntaje:     puntaje,
	}
}

func (m *ProductivityIndicator) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
