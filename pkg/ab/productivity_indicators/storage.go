package productivity_indicators

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesProductivityIndicatorRepository interface {
	create(m *ProductivityIndicator) error
	update(m *ProductivityIndicator) error
	delete(id int) error
	getByID(id int) (*ProductivityIndicator, error)
	getAll() ([]*ProductivityIndicator, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesProductivityIndicatorRepository {
	var s ServicesProductivityIndicatorRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newProductivityIndicatorSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
