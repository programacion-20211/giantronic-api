package productivity_indicators

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerProductivityIndicator interface {
	CreateProductivityIndicator(tipoPuntaje string, puntaje int) (*ProductivityIndicator, int, error)
	UpdateProductivityIndicator(id int, tipoPuntaje string, puntaje int) (*ProductivityIndicator, int, error)
	DeleteProductivityIndicator(id int) (int, error)
	GetProductivityIndicatorByID(id int) (*ProductivityIndicator, int, error)
	GetAllProductivityIndicator() ([]*ProductivityIndicator, error)
}

type service struct {
	repository ServicesProductivityIndicatorRepository
	user       *models.User
	txID       string
}

func NewProductivityIndicatorService(repository ServicesProductivityIndicatorRepository, user *models.User, TxID string) PortsServerProductivityIndicator {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateProductivityIndicator(tipoPuntaje string, puntaje int) (*ProductivityIndicator, int, error) {
	m := NewCreateProductivityIndicator(tipoPuntaje, puntaje)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create ProductivityIndicator :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateProductivityIndicator(id int, tipoPuntaje string, puntaje int) (*ProductivityIndicator, int, error) {
	m := NewProductivityIndicator(id, tipoPuntaje, puntaje)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update ProductivityIndicator :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteProductivityIndicator(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetProductivityIndicatorByID(id int) (*ProductivityIndicator, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllProductivityIndicator() ([]*ProductivityIndicator, error) {
	return s.repository.getAll()
}
