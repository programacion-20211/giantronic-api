package niveles_servicio

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// NivelServicio  Model struct NivelServicio
type NivelServicio struct {
	ID                   int       `json:"id" db:"id" valid:"-"`
	PeriodoId            int       `json:"periodo_id" db:"periodo_id" valid:"required"`
	ValorEstandar        int       `json:"valor_estandar" db:"valor_estandar" valid:"required"`
	ValorCritico         int       `json:"valor_critico" db:"valor_critico" valid:"required"`
	CasosResueltosTiempo int       `json:"casos_resueltos_tiempo" db:"casos_resueltos_tiempo" valid:"required"`
	CasosAbiertos        int       `json:"casos_abiertos" db:"casos_abiertos" valid:"required"`
	CasosMesAnterior     int       `json:"casos_mes_anterior" db:"casos_mes_anterior" valid:"required"`
	CasosMesSiguiente    int       `json:"casos_mes_siguiente" db:"casos_mes_siguiente" valid:"required"`
	MetasCumplidas       int       `json:"metas_cumplidas" db:"metas_cumplidas" valid:"required"`
	PuntajeC             int       `json:"puntaje_c" db:"puntaje_c" valid:"required"`
	PuntajeD             int       `json:"puntaje_d" db:"puntaje_d" valid:"required"`
	Puntos               int       `json:"puntos" db:"puntos" valid:"required"`
	CreatedAt            time.Time `json:"created_at" db:"created_at"`
	UpdatedAt            time.Time `json:"updated_at" db:"updated_at"`
}

func NewNivelServicio(id int, periodoId int, valorEstandar int, valorCritico int, casosResueltosTiempo int, casosAbiertos int, casosMesAnterior int, casosMesSiguiente int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) *NivelServicio {
	return &NivelServicio{
		ID:                   id,
		PeriodoId:            periodoId,
		ValorEstandar:        valorEstandar,
		ValorCritico:         valorCritico,
		CasosResueltosTiempo: casosResueltosTiempo,
		CasosAbiertos:        casosAbiertos,
		CasosMesAnterior:     casosMesAnterior,
		CasosMesSiguiente:    casosMesSiguiente,
		MetasCumplidas:       metasCumplidas,
		PuntajeC:             puntajeC,
		PuntajeD:             puntajeD,
		Puntos:               puntos,
	}
}

func NewCreateNivelServicio(periodoId int, valorEstandar int, valorCritico int, casosResueltosTiempo int, casosAbiertos int, casosMesAnterior int, casosMesSiguiente int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) *NivelServicio {
	return &NivelServicio{
		PeriodoId:            periodoId,
		ValorEstandar:        valorEstandar,
		ValorCritico:         valorCritico,
		CasosResueltosTiempo: casosResueltosTiempo,
		CasosAbiertos:        casosAbiertos,
		CasosMesAnterior:     casosMesAnterior,
		CasosMesSiguiente:    casosMesSiguiente,
		MetasCumplidas:       metasCumplidas,
		PuntajeC:             puntajeC,
		PuntajeD:             puntajeD,
		Puntos:               puntos,
	}
}

func (m *NivelServicio) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
