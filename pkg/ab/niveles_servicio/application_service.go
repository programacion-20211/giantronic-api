package niveles_servicio

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerNivelServicio interface {
	CreateNivelServicio(periodoId int, valorEstandar int, valorCritico int, casosResueltosTiempo int, casosAbiertos int, casosMesAnterior int, casosMesSiguiente int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) (*NivelServicio, int, error)
	UpdateNivelServicio(id int, periodoId int, valorEstandar int, valorCritico int, casosResueltosTiempo int, casosAbiertos int, casosMesAnterior int, casosMesSiguiente int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) (*NivelServicio, int, error)
	DeleteNivelServicio(id int) (int, error)
	GetNivelServicioByID(id int) (*NivelServicio, int, error)
	GetAllNivelServicio() ([]*NivelServicio, error)
}

type service struct {
	repository ServicesNivelServicioRepository
	user       *models.User
	txID       string
}

func NewNivelServicioService(repository ServicesNivelServicioRepository, user *models.User, TxID string) PortsServerNivelServicio {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateNivelServicio(periodoId int, valorEstandar int, valorCritico int, casosResueltosTiempo int, casosAbiertos int, casosMesAnterior int, casosMesSiguiente int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) (*NivelServicio, int, error) {
	m := NewCreateNivelServicio(periodoId, valorEstandar, valorCritico, casosResueltosTiempo, casosAbiertos, casosMesAnterior, casosMesSiguiente, metasCumplidas, puntajeC, puntajeD, puntos)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create NivelServicio :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateNivelServicio(id int, periodoId int, valorEstandar int, valorCritico int, casosResueltosTiempo int, casosAbiertos int, casosMesAnterior int, casosMesSiguiente int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) (*NivelServicio, int, error) {
	m := NewNivelServicio(id, periodoId, valorEstandar, valorCritico, casosResueltosTiempo, casosAbiertos, casosMesAnterior, casosMesSiguiente, metasCumplidas, puntajeC, puntajeD, puntos)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update NivelServicio :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteNivelServicio(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetNivelServicioByID(id int) (*NivelServicio, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllNivelServicio() ([]*NivelServicio, error) {
	return s.repository.getAll()
}
