package niveles_servicio

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

// sqlServer estructura de conexión a la BD de mssql
type sqlserver struct {
	DB   *sqlx.DB
	user *models.User
	TxID string
}

func newNivelServicioSqlServerRepository(db *sqlx.DB, user *models.User, txID string) *sqlserver {
	return &sqlserver{
		DB:   db,
		user: user,
		TxID: txID,
	}
}

// Create registra en la BD
func (s *sqlserver) create(m *NivelServicio) error {
	var id int
	date := time.Now()
	m.UpdatedAt = date
	m.CreatedAt = date
	const sqlInsert = `INSERT INTO ab.niveles_servicio (periodo_id, valor_estandar, valor_critico, casos_resueltos_tiempo, casos_abiertos, casos_mes_anterior, casos_mes_siguiente, metas_cumplidas, puntaje_c, puntaje_d, puntos, created_at, updated_at) VALUES (@periodo_id, @valor_estandar, @valor_critico, @casos_resueltos_tiempo, @casos_abiertos, @casos_mes_anterior, @casos_mes_siguiente, @metas_cumplidas, @puntaje_c, @puntaje_d, @puntos, @created_at, @updated_at) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	stmt, err := s.DB.Prepare(sqlInsert)
	if err != nil {
		return err
	}
	defer stmt.Close()
	err = stmt.QueryRow(
		sql.Named("periodo_id", m.PeriodoId),
		sql.Named("valor_estandar", m.ValorEstandar),
		sql.Named("valor_critico", m.ValorCritico),
		sql.Named("casos_resueltos_tiempo", m.CasosResueltosTiempo),
		sql.Named("casos_abiertos", m.CasosAbiertos),
		sql.Named("casos_mes_anterior", m.CasosMesAnterior),
		sql.Named("casos_mes_siguiente", m.CasosMesSiguiente),
		sql.Named("metas_cumplidas", m.MetasCumplidas),
		sql.Named("puntaje_c", m.PuntajeC),
		sql.Named("puntaje_d", m.PuntajeD),
		sql.Named("puntos", m.Puntos),
		sql.Named("created_at", m.CreatedAt),
		sql.Named("updated_at", m.UpdatedAt),
	).Scan(&id)
	if err != nil {
		return err
	}
	m.ID = int(id)
	return nil
}

// Update actualiza un registro en la BD
func (s *sqlserver) update(m *NivelServicio) error {
	date := time.Now()
	m.UpdatedAt = date
	const sqlUpdate = `UPDATE ab.niveles_servicio SET periodo_id = :periodo_id, valor_estandar = :valor_estandar, valor_critico = :valor_critico, casos_resueltos_tiempo = :casos_resueltos_tiempo, casos_abiertos = :casos_abiertos, casos_mes_anterior = :casos_mes_anterior, casos_mes_siguiente = :casos_mes_siguiente, metas_cumplidas = :metas_cumplidas, puntaje_c = :puntaje_c, puntaje_d = :puntaje_d, puntos = :puntos, updated_at = :updated_at WHERE id = :id `
	rs, err := s.DB.NamedExec(sqlUpdate, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// Delete elimina un registro de la BD
func (s *sqlserver) delete(id int) error {
	const sqlDelete = `DELETE FROM ab.niveles_servicio WHERE id = :id `
	m := NivelServicio{ID: id}
	rs, err := s.DB.NamedExec(sqlDelete, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// GetByID consulta un registro por su ID
func (s *sqlserver) getByID(id int) (*NivelServicio, error) {
	const sqlGetByID = `SELECT convert(nvarchar(50), id) id , periodo_id, valor_estandar, valor_critico, casos_resueltos_tiempo, casos_abiertos, casos_mes_anterior, casos_mes_siguiente, metas_cumplidas, puntaje_c, puntaje_d, puntos, created_at, updated_at FROM ab.niveles_servicio  WITH (NOLOCK)  WHERE id = @id `
	mdl := NivelServicio{}
	err := s.DB.Get(&mdl, sqlGetByID, sql.Named("id", id))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return &mdl, err
	}
	return &mdl, nil
}

// GetAll consulta todos los registros de la BD
func (s *sqlserver) getAll() ([]*NivelServicio, error) {
	var ms []*NivelServicio
	const sqlGetAll = `SELECT convert(nvarchar(50), id) id , periodo_id, valor_estandar, valor_critico, casos_resueltos_tiempo, casos_abiertos, casos_mes_anterior, casos_mes_siguiente, metas_cumplidas, puntaje_c, puntaje_d, puntos, created_at, updated_at FROM ab.niveles_servicio  WITH (NOLOCK) `

	err := s.DB.Select(&ms, sqlGetAll)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return ms, err
	}
	return ms, nil
}
