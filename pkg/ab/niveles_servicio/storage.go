package niveles_servicio

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesNivelServicioRepository interface {
	create(m *NivelServicio) error
	update(m *NivelServicio) error
	delete(id int) error
	getByID(id int) (*NivelServicio, error)
	getAll() ([]*NivelServicio, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesNivelServicioRepository {
	var s ServicesNivelServicioRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newNivelServicioSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
