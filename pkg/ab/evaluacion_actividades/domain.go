package evaluacion_actividades

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// EvaluacionActividad  Model struct EvaluacionActividad
type EvaluacionActividad struct {
	ID         int       `json:"id" db:"id" valid:"-"`
	IdEmpleado int       `json:"id_empleado" db:"id_empleado" valid:"required"`
	PeriodoId  int       `json:"periodo_id" db:"periodo_id" valid:"required"`
	CreatedAt  time.Time `json:"created_at" db:"created_at"`
	UpdatedAt  time.Time `json:"updated_at" db:"updated_at"`
}

func NewEvaluacionActividad(id int, idEmpleado int, periodoId int) *EvaluacionActividad {
	return &EvaluacionActividad{
		ID:         id,
		IdEmpleado: idEmpleado,
		PeriodoId:  periodoId,
	}
}

func NewCreateEvaluacionActividad(idEmpleado int, periodoId int) *EvaluacionActividad {
	return &EvaluacionActividad{
		IdEmpleado: idEmpleado,
		PeriodoId:  periodoId,
	}
}

func (m *EvaluacionActividad) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
