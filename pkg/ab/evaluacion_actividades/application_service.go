package evaluacion_actividades

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerEvaluacionActividad interface {
	CreateEvaluacionActividad(idEmpleado int, periodoId int) (*EvaluacionActividad, int, error)
	UpdateEvaluacionActividad(id int, idEmpleado int, periodoId int) (*EvaluacionActividad, int, error)
	DeleteEvaluacionActividad(id int) (int, error)
	GetEvaluacionActividadByID(id int) (*EvaluacionActividad, int, error)
	GetAllEvaluacionActividad() ([]*EvaluacionActividad, error)
	GetAllEvaluacionByPeriodo(periodo int) ([]*EvaluacionActividad, int, error)
}

type service struct {
	repository ServicesEvaluacionActividadRepository
	user       *models.User
	txID       string
}

func NewEvaluacionActividadService(repository ServicesEvaluacionActividadRepository, user *models.User, TxID string) PortsServerEvaluacionActividad {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateEvaluacionActividad(idEmpleado int, periodoId int) (*EvaluacionActividad, int, error) {
	m := NewCreateEvaluacionActividad(idEmpleado, periodoId)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create EvaluacionActividad :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateEvaluacionActividad(id int, idEmpleado int, periodoId int) (*EvaluacionActividad, int, error) {
	m := NewEvaluacionActividad(id, idEmpleado, periodoId)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update EvaluacionActividad :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteEvaluacionActividad(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetEvaluacionActividadByID(id int) (*EvaluacionActividad, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllEvaluacionActividad() ([]*EvaluacionActividad, error) {
	return s.repository.getAll()
}

func (s *service) GetAllEvaluacionByPeriodo(periodo int) ([]*EvaluacionActividad, int, error) {
	if periodo == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("periodo is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByPeriodo(periodo)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}
