package evaluacion_actividades

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesEvaluacionActividadRepository interface {
	create(m *EvaluacionActividad) error
	update(m *EvaluacionActividad) error
	delete(id int) error
	getByID(id int) (*EvaluacionActividad, error)
	getAll() ([]*EvaluacionActividad, error)
	getByPeriodo(periodo int) ([]*EvaluacionActividad, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesEvaluacionActividadRepository {
	var s ServicesEvaluacionActividadRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newEvaluacionActividadSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
