package cargos

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesCargoRepository interface {
	create(m *Cargo) error
	update(m *Cargo) error
	delete(id int) error
	getByID(id int) (*Cargo, error)
	getAll() ([]*Cargo, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesCargoRepository {
	var s ServicesCargoRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newCargoSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
