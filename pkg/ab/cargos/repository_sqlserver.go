package cargos

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

// sqlServer estructura de conexión a la BD de mssql
type sqlserver struct {
	DB   *sqlx.DB
	user *models.User
	TxID string
}

func newCargoSqlServerRepository(db *sqlx.DB, user *models.User, txID string) *sqlserver {
	return &sqlserver{
		DB:   db,
		user: user,
		TxID: txID,
	}
}

// Create registra en la BD
func (s *sqlserver) create(m *Cargo) error {
	var id int
	date := time.Now()
	m.UpdatedAt = date
	m.CreatedAt = date
	const sqlInsert = `INSERT INTO ab.cargos (cargo, porcetaje_cumplimiento_actividades, porcetaje_mesa_ayuda, porcetaje_disponibilidad_servicio, porcetaje_niveles_servicio, created_at, updated_at) VALUES (@cargo, @porcetaje_cumplimiento_actividades, @porcetaje_mesa_ayuda, @porcetaje_disponibilidad_servicio, @porcetaje_niveles_servicio, @created_at, @updated_at) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	stmt, err := s.DB.Prepare(sqlInsert)
	if err != nil {
		return err
	}
	defer stmt.Close()
	err = stmt.QueryRow(
		sql.Named("cargo", m.Cargo),
		sql.Named("porcetaje_cumplimiento_actividades", m.PorcetajeCumplimientoActividades),
		sql.Named("porcetaje_mesa_ayuda", m.PorcetajeMesaAyuda),
		sql.Named("porcetaje_disponibilidad_servicio", m.PorcetajeDisponibilidadServicio),
		sql.Named("porcetaje_niveles_servicio", m.PorcetajeNivelesServicio),
		sql.Named("created_at", m.CreatedAt),
		sql.Named("updated_at", m.UpdatedAt),
	).Scan(&id)
	if err != nil {
		return err
	}
	m.ID = int(id)
	return nil
}

// Update actualiza un registro en la BD
func (s *sqlserver) update(m *Cargo) error {
	date := time.Now()
	m.UpdatedAt = date
	const sqlUpdate = `UPDATE ab.cargos SET cargo = :cargo, porcetaje_cumplimiento_actividades = :porcetaje_cumplimiento_actividades, porcetaje_mesa_ayuda = :porcetaje_mesa_ayuda, porcetaje_disponibilidad_servicio = :porcetaje_disponibilidad_servicio, porcetaje_niveles_servicio = :porcetaje_niveles_servicio, updated_at = :updated_at WHERE id = :id `
	rs, err := s.DB.NamedExec(sqlUpdate, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// Delete elimina un registro de la BD
func (s *sqlserver) delete(id int) error {
	const sqlDelete = `DELETE FROM ab.cargos WHERE id = :id `
	m := Cargo{ID: id}
	rs, err := s.DB.NamedExec(sqlDelete, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// GetByID consulta un registro por su ID
func (s *sqlserver) getByID(id int) (*Cargo, error) {
	const sqlGetByID = `SELECT convert(nvarchar(50), id) id , cargo, porcetaje_cumplimiento_actividades, porcetaje_mesa_ayuda, porcetaje_disponibilidad_servicio, porcetaje_niveles_servicio, created_at, updated_at FROM ab.cargos  WITH (NOLOCK)  WHERE id = @id `
	mdl := Cargo{}
	err := s.DB.Get(&mdl, sqlGetByID, sql.Named("id", id))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return &mdl, err
	}
	return &mdl, nil
}

// GetAll consulta todos los registros de la BD
func (s *sqlserver) getAll() ([]*Cargo, error) {
	var ms []*Cargo
	const sqlGetAll = `SELECT convert(nvarchar(50), id) id , cargo, porcetaje_cumplimiento_actividades, porcetaje_mesa_ayuda, porcetaje_disponibilidad_servicio, porcetaje_niveles_servicio, created_at, updated_at FROM ab.cargos  WITH (NOLOCK) `

	err := s.DB.Select(&ms, sqlGetAll)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return ms, err
	}
	return ms, nil
}
