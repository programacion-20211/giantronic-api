package cargos

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerCargo interface {
	CreateCargo(cargo string, porcetajeCumplimientoActividades int, porcetajeMesaAyuda int, porcetajeDisponibilidadServicio int, porcetajeNivelesServicio int) (*Cargo, int, error)
	UpdateCargo(id int, cargo string, porcetajeCumplimientoActividades int, porcetajeMesaAyuda int, porcetajeDisponibilidadServicio int, porcetajeNivelesServicio int) (*Cargo, int, error)
	DeleteCargo(id int) (int, error)
	GetCargoByID(id int) (*Cargo, int, error)
	GetAllCargo() ([]*Cargo, error)
}

type service struct {
	repository ServicesCargoRepository
	user       *models.User
	txID       string
}

func NewCargoService(repository ServicesCargoRepository, user *models.User, TxID string) PortsServerCargo {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateCargo(cargo string, porcetajeCumplimientoActividades int, porcetajeMesaAyuda int, porcetajeDisponibilidadServicio int, porcetajeNivelesServicio int) (*Cargo, int, error) {
	m := NewCreateCargo(cargo, porcetajeCumplimientoActividades, porcetajeMesaAyuda, porcetajeDisponibilidadServicio, porcetajeNivelesServicio)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create Cargo :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateCargo(id int, cargo string, porcetajeCumplimientoActividades int, porcetajeMesaAyuda int, porcetajeDisponibilidadServicio int, porcetajeNivelesServicio int) (*Cargo, int, error) {
	m := NewCargo(id, cargo, porcetajeCumplimientoActividades, porcetajeMesaAyuda, porcetajeDisponibilidadServicio, porcetajeNivelesServicio)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update Cargo :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteCargo(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetCargoByID(id int) (*Cargo, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllCargo() ([]*Cargo, error) {
	return s.repository.getAll()
}
