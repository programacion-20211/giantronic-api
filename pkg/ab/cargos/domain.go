package cargos

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// Cargo  Model struct Cargo
type Cargo struct {
	ID                               int       `json:"id" db:"id" valid:"-"`
	Cargo                            string    `json:"cargo" db:"cargo" valid:"required"`
	PorcetajeCumplimientoActividades int       `json:"porcetaje_cumplimiento_actividades" db:"porcetaje_cumplimiento_actividades" valid:"required"`
	PorcetajeMesaAyuda               int       `json:"porcetaje_mesa_ayuda" db:"porcetaje_mesa_ayuda" valid:"required"`
	PorcetajeDisponibilidadServicio  int       `json:"porcetaje_disponibilidad_servicio" db:"porcetaje_disponibilidad_servicio" valid:"required"`
	PorcetajeNivelesServicio         int       `json:"porcetaje_niveles_servicio" db:"porcetaje_niveles_servicio" valid:"required"`
	CreatedAt                        time.Time `json:"created_at" db:"created_at"`
	UpdatedAt                        time.Time `json:"updated_at" db:"updated_at"`
}

func NewCargo(id int, cargo string, porcetajeCumplimientoActividades int, porcetajeMesaAyuda int, porcetajeDisponibilidadServicio int, porcetajeNivelesServicio int) *Cargo {
	return &Cargo{
		ID:                               id,
		Cargo:                            cargo,
		PorcetajeCumplimientoActividades: porcetajeCumplimientoActividades,
		PorcetajeMesaAyuda:               porcetajeMesaAyuda,
		PorcetajeDisponibilidadServicio:  porcetajeDisponibilidadServicio,
		PorcetajeNivelesServicio:         porcetajeNivelesServicio,
	}
}

func NewCreateCargo(cargo string, porcetajeCumplimientoActividades int, porcetajeMesaAyuda int, porcetajeDisponibilidadServicio int, porcetajeNivelesServicio int) *Cargo {
	return &Cargo{
		Cargo:                            cargo,
		PorcetajeCumplimientoActividades: porcetajeCumplimientoActividades,
		PorcetajeMesaAyuda:               porcetajeMesaAyuda,
		PorcetajeDisponibilidadServicio:  porcetajeDisponibilidadServicio,
		PorcetajeNivelesServicio:         porcetajeNivelesServicio,
	}
}

func (m *Cargo) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
