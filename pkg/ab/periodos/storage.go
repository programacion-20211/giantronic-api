package periodos

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesPeriodoRepository interface {
	create(m *Periodo) error
	update(m *Periodo) error
	delete(id int) error
	getByID(id int) (*Periodo, error)
	getAll() ([]*Periodo, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesPeriodoRepository {
	var s ServicesPeriodoRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newPeriodoSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
