package periodos

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerPeriodo interface {
	CreatePeriodo(mes int, año int) (*Periodo, int, error)
	UpdatePeriodo(id int, mes int, año int) (*Periodo, int, error)
	DeletePeriodo(id int) (int, error)
	GetPeriodoByID(id int) (*Periodo, int, error)
	GetAllPeriodo() ([]*Periodo, error)
}

type service struct {
	repository ServicesPeriodoRepository
	user       *models.User
	txID       string
}

func NewPeriodoService(repository ServicesPeriodoRepository, user *models.User, TxID string) PortsServerPeriodo {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreatePeriodo(mes int, año int) (*Periodo, int, error) {
	m := NewCreatePeriodo(mes, año)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create Periodo :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdatePeriodo(id int, mes int, año int) (*Periodo, int, error) {
	m := NewPeriodo(id, mes, año)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update Periodo :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeletePeriodo(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetPeriodoByID(id int) (*Periodo, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllPeriodo() ([]*Periodo, error) {
	return s.repository.getAll()
}
