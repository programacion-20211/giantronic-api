package periodos

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// Periodo  Model struct Periodo
type Periodo struct {
	ID        int       `json:"id" db:"id" valid:"-"`
	Mes       int       `json:"mes" db:"mes" valid:"required"`
	Año       int       `json:"año" db:"año" valid:"required"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
}

func NewPeriodo(id int, mes int, año int) *Periodo {
	return &Periodo{
		ID:  id,
		Mes: mes,
		Año: año,
	}
}

func NewCreatePeriodo(mes int, año int) *Periodo {
	return &Periodo{
		Mes: mes,
		Año: año,
	}
}

func (m *Periodo) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
