package usuarios

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

// sqlServer estructura de conexión a la BD de mssql
type sqlserver struct {
	DB   *sqlx.DB
	user *models.User
	TxID string
}

func newUsuarioSqlServerRepository(db *sqlx.DB, user *models.User, txID string) *sqlserver {
	return &sqlserver{
		DB:   db,
		user: user,
		TxID: txID,
	}
}

// Create registra en la BD
func (s *sqlserver) create(m *Usuario) error {

	const sqlInsert = `INSERT INTO dbo.TB_USUARIOS (dni, nombre, apellidop, apellidom, correo, telefono, direccion, nick, contraseña, perfil) VALUES (@dni, @nombre, @apellidop, @apellidom, @correo, @telefono, @direccion, @nick, @contrasena, @perfil)  `
	stmt, err := s.DB.Prepare(sqlInsert)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(
		sql.Named("dni", m.DNI),
		sql.Named("nombre", m.Nombre),
		sql.Named("apellidop", m.Apellidop),
		sql.Named("apellidom", m.Apellidom),
		sql.Named("correo", m.Correo),
		sql.Named("telefono", m.Telefono),
		sql.Named("direccion", m.Direccion),
		sql.Named("nick", m.Nick),
		sql.Named("contraseña", m.Contrasena),
		sql.Named("perfil", m.Perfil),
	)
	if err != nil {
		return err
	}
	return nil
}

// Update actualiza un registro en la BD
func (s *sqlserver) update(m *Usuario) error {

	const sqlUpdate = `UPDATE dbo.TB_USUARIOS SET dni = :dni, nombre = :nombre, apellidop = :apellidop, apellidom = :apellidom, correo = :correo, telefono = :telefono, direccion =:direccion, nick = :nick, perfil = :perfil WHERE dni = :dni `
	rs, err := s.DB.NamedExec(sqlUpdate, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// Delete elimina un registro de la BD
func (s *sqlserver) delete(dni string) error {
	const sqlDelete = `DELETE FROM dbo.TB_USUARIOS WHERE dni=:dni `
	m := Usuario{DNI: dni}
	rs, err := s.DB.NamedExec(sqlDelete, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}

// GetByID consulta un registro por su ID
func (s *sqlserver) getByID(dni string) (*Usuario, error) {
	const sqlGetByID = `SELECT dni, nombre, apellidop, apellidom, correo, telefono, direccion, nick, contraseña as contrasena, perfil FROM dbo.TB_USUARIOS  WITH (NOLOCK)  WHERE dni = @dni `
	mdl := Usuario{}
	err := s.DB.Get(&mdl, sqlGetByID, sql.Named("dni", dni))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return &mdl, err
	}
	return &mdl, nil
}

// GetAll consulta todos los registros de la BD
func (s *sqlserver) getAll() ([]*Usuario, error) {
	var ms []*Usuario
	const sqlGetAll = `SELECT dni, nombre, apellidop, apellidom, correo, telefono, direccion, nick, contrasena as contrasena, perfil FROM dbo.TB_USUARIOS  WITH (NOLOCK) `

	err := s.DB.Select(&ms, sqlGetAll)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return ms, err
	}
	return ms, nil
}

func (s *sqlserver) getByNick(nick string) (*Usuario, error) {
	const sqlGetByEmail = `SELECT dni, nombre, apellidop, apellidom, correo, telefono, direccion, nick, contraseña as contrasena, perfil FROM dbo.TB_USUARIOS  WITH (NOLOCK)  WHERE nick = @nick `
	mdl := Usuario{}
	err := s.DB.Get(&mdl, sqlGetByEmail, sql.Named("nick", nick))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return &mdl, err
	}
	return &mdl, nil
}

// Update actualiza un registro en la BD
func (s *sqlserver) updatePassword(dni, contrasena string) error {
	type data struct {
		Dni        string `json:"dni" db:"dni" valid:"-"`
		Contrasena string `json:"contraseña" db:"contrasena" valid:"-"`
	}
	m := data{
		Dni:        dni,
		Contrasena: contrasena,
	}
	const sqlUpdate = `UPDATE dbo.TB_USUARIOS SET contraseña= :contrasena WHERE dni = :dni `
	rs, err := s.DB.NamedExec(sqlUpdate, &m)
	if err != nil {
		return err
	}
	if i, _ := rs.RowsAffected(); i == 0 {
		return fmt.Errorf("ecatch:108")
	}
	return nil
}
