package usuarios

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerUsuario interface {
	CreateUsuario(DNI, Correo, Nombre, Apellidop, Apellidom, Telefono, Direccion, Nick, Contrasena, Perfil string) (*Usuario, int, error)
	UpdateUsuario(DNI, Correo, Nombre, Apellidop, Apellidom, Telefono, Direccion, Nick, Contrasena, Perfil string) (*Usuario, int, error)
	DeleteUsuario(id string) (int, error)
	GetUsuarioByID(id string) (*Usuario, int, error)
	GetAllUsuario() ([]*Usuario, error)
	GetUsuarioByNick(nick string) (*Usuario, int, error)
	ResetFieldsUser(usr *Usuario) *models.User
	UpdatePassword(DNI, Contrasena string) (int, error)
}

type service struct {
	repository ServicesUsuarioRepository
	user       *models.User
	txID       string
}

func NewUsuarioService(repository ServicesUsuarioRepository, user *models.User, TxID string) PortsServerUsuario {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateUsuario(DNI, Correo, Nombre, Apellidop, Apellidom, Telefono, Direccion, Nick, Contrasena, Perfil string) (*Usuario, int, error) {
	m := NewCreateUsuariofunc(DNI, Correo, Nombre, Apellidop, Apellidom, Telefono, Direccion, Nick, Contrasena, Perfil)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create Usuario :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateUsuario(DNI, Correo, Nombre, Apellidop, Apellidom, Telefono, Direccion, Nick, Contrasena, Perfil string) (*Usuario, int, error) {
	m := NewUsuario(DNI, Correo, Nombre, Apellidop, Apellidom, Telefono, Direccion, Nick, Contrasena, Perfil)
	if DNI == "" {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update Usuario :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteUsuario(id string) (int, error) {
	if id == "" {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetUsuarioByID(id string) (*Usuario, int, error) {
	if id == "" {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllUsuario() ([]*Usuario, error) {
	return s.repository.getAll()
}

func (s *service) GetUsuarioByNick(nick string) (*Usuario, int, error) {
	if nick == "" {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("nick is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByNick(nick)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByNick row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) ResetFieldsUser(usr *Usuario) *models.User {
	return &models.User{
		DNI:        usr.DNI,
		Correo:     usr.Correo,
		Nombre:     usr.Nombre,
		Apellidop:  usr.Apellidop,
		Apellidom:  usr.Apellidom,
		Telefono:   usr.Telefono,
		Direccion:  usr.Direccion,
		Nick:       usr.Nick,
		Contrasena: "",
	}

}

func (s *service) UpdatePassword(DNI, Contrasena string) (int, error) {

	if err := s.repository.updatePassword(DNI, Contrasena); err != nil {
		logger.Error.Println(s.txID, " - couldn't update Password :", err)
		return 18, err
	}
	return 29, nil
}
