package usuarios

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	Postgresql = "postgres"
	SqlServer  = "sqlserver"
	Oracle     = "oci8"
)

type ServicesUsuarioRepository interface {
	create(m *Usuario) error
	update(m *Usuario) error
	updatePassword(dni, password string) error
	delete(id string) error
	getByID(id string) (*Usuario, error)
	getAll() ([]*Usuario, error)
	getByNick(nick string) (*Usuario, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesUsuarioRepository {
	var s ServicesUsuarioRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newUsuarioSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
