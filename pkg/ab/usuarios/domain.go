package usuarios

import (
	"github.com/asaskevich/govalidator"
)

// Usuario  Model struct Usuario
type Usuario struct {
	DNI        string `json:"dni" db:"dni" valid:"-"`
	Correo     string `json:"correo" db:"correo" valid:"-"`
	Nombre     string `json:"nombre" db:"nombre" valid:"required"`
	Apellidop  string `json:"apellidop" db:"apellidop" valid:"required"`
	Apellidom  string `json:"apellidom" db:"apellidom" valid:"required"`
	Telefono   string `json:"telefono" db:"telefono" valid:"required"`
	Direccion  string `json:"direccion" db:"direccion" valid:"required"`
	Nick       string `json:"nick" db:"nick" valid:"required"`
	Contrasena string `json:"contrasena" db:"contrasena" valid:"required"`
	Perfil     string `json:"perfil" db:"perfil" valid:"required"`
}

func NewUsuario(DNI, Correo, Nombre, Apellidop, Apellidom, Telefono, Direccion, Nick, Contrasena, Perfil string) *Usuario {
	return &Usuario{
		DNI:        DNI,
		Correo:     Correo,
		Nombre:     Nombre,
		Apellidop:  Apellidop,
		Apellidom:  Apellidom,
		Telefono:   Telefono,
		Direccion:  Direccion,
		Nick:       Nick,
		Contrasena: Contrasena,
		Perfil:     Perfil,
	}
}

func NewCreateUsuariofunc(DNI, Correo, Nombre, Apellidop, Apellidom, Telefono, Direccion, Nick, Contrasena, Perfil string) *Usuario {
	return &Usuario{
		DNI:        DNI,
		Correo:     Correo,
		Nombre:     Nombre,
		Apellidop:  Apellidop,
		Apellidom:  Apellidom,
		Telefono:   Telefono,
		Direccion:  Direccion,
		Nick:       Nick,
		Contrasena: Contrasena,
		Perfil:     Perfil,
	}
}

func (m *Usuario) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
