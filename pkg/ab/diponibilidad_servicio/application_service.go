package diponibilidad_servicio

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerDiponibilidadServicio interface {
	CreateDiponibilidadServicio(periodoId int, valorEstandar int, valorCritico int, horasTotales int, horasFallo int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) (*DiponibilidadServicio, int, error)
	UpdateDiponibilidadServicio(id int, periodoId int, valorEstandar int, valorCritico int, horasTotales int, horasFallo int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) (*DiponibilidadServicio, int, error)
	DeleteDiponibilidadServicio(id int) (int, error)
	GetDiponibilidadServicioByID(id int) (*DiponibilidadServicio, int, error)
	GetAllDiponibilidadServicio() ([]*DiponibilidadServicio, error)
}

type service struct {
	repository ServicesDiponibilidadServicioRepository
	user       *models.User
	txID       string
}

func NewDiponibilidadServicioService(repository ServicesDiponibilidadServicioRepository, user *models.User, TxID string) PortsServerDiponibilidadServicio {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateDiponibilidadServicio(periodoId int, valorEstandar int, valorCritico int, horasTotales int, horasFallo int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) (*DiponibilidadServicio, int, error) {
	m := NewCreateDiponibilidadServicio(periodoId, valorEstandar, valorCritico, horasTotales, horasFallo, metasCumplidas, puntajeC, puntajeD, puntos)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create DiponibilidadServicio :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateDiponibilidadServicio(id int, periodoId int, valorEstandar int, valorCritico int, horasTotales int, horasFallo int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) (*DiponibilidadServicio, int, error) {
	m := NewDiponibilidadServicio(id, periodoId, valorEstandar, valorCritico, horasTotales, horasFallo, metasCumplidas, puntajeC, puntajeD, puntos)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update DiponibilidadServicio :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteDiponibilidadServicio(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetDiponibilidadServicioByID(id int) (*DiponibilidadServicio, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllDiponibilidadServicio() ([]*DiponibilidadServicio, error) {
	return s.repository.getAll()
}
