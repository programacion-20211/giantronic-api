package diponibilidad_servicio

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesDiponibilidadServicioRepository interface {
	create(m *DiponibilidadServicio) error
	update(m *DiponibilidadServicio) error
	delete(id int) error
	getByID(id int) (*DiponibilidadServicio, error)
	getAll() ([]*DiponibilidadServicio, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesDiponibilidadServicioRepository {
	var s ServicesDiponibilidadServicioRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newDiponibilidadServicioSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
