package diponibilidad_servicio

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// DiponibilidadServicio  Model struct DiponibilidadServicio
type DiponibilidadServicio struct {
	ID             int       `json:"id" db:"id" valid:"-"`
	PeriodoId      int       `json:"periodo_id" db:"periodo_id" valid:"required"`
	ValorEstandar  int       `json:"valor_estandar" db:"valor_estandar" valid:"required"`
	ValorCritico   int       `json:"valor_critico" db:"valor_critico" valid:"required"`
	HorasTotales   int       `json:"horas_totales" db:"horas_totales" valid:"required"`
	HorasFallo     int       `json:"horas_fallo" db:"horas_fallo" valid:"required"`
	MetasCumplidas int       `json:"metas_cumplidas" db:"metas_cumplidas" valid:"required"`
	PuntajeC       int       `json:"puntaje_c" db:"puntaje_c" valid:"required"`
	PuntajeD       int       `json:"puntaje_d" db:"puntaje_d" valid:"required"`
	Puntos         int       `json:"puntos" db:"puntos" valid:"required"`
	CreatedAt      time.Time `json:"created_at" db:"created_at"`
	UpdatedAt      time.Time `json:"updated_at" db:"updated_at"`
}

func NewDiponibilidadServicio(id int, periodoId int, valorEstandar int, valorCritico int, horasTotales int, horasFallo int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) *DiponibilidadServicio {
	return &DiponibilidadServicio{
		ID:             id,
		PeriodoId:      periodoId,
		ValorEstandar:  valorEstandar,
		ValorCritico:   valorCritico,
		HorasTotales:   horasTotales,
		HorasFallo:     horasFallo,
		MetasCumplidas: metasCumplidas,
		PuntajeC:       puntajeC,
		PuntajeD:       puntajeD,
		Puntos:         puntos,
	}
}

func NewCreateDiponibilidadServicio(periodoId int, valorEstandar int, valorCritico int, horasTotales int, horasFallo int, metasCumplidas int, puntajeC int, puntajeD int, puntos int) *DiponibilidadServicio {
	return &DiponibilidadServicio{
		PeriodoId:      periodoId,
		ValorEstandar:  valorEstandar,
		ValorCritico:   valorCritico,
		HorasTotales:   horasTotales,
		HorasFallo:     horasFallo,
		MetasCumplidas: metasCumplidas,
		PuntajeC:       puntajeC,
		PuntajeD:       puntajeD,
		Puntos:         puntos,
	}
}

func (m *DiponibilidadServicio) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
