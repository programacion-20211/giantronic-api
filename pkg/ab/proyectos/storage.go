package proyectos

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	Postgresql = "postgres"
	SqlServer  = "sqlserver"
	Oracle     = "oci8"
)

type ServicesProyectoRepository interface {
	create(m *Proyecto) error
	update(m *Proyecto) error
	delete(id int) error
	getByID(id int) (*Proyecto, error)
	getAll() ([]*Proyecto, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesProyectoRepository {
	var s ServicesProyectoRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newProyectoSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
