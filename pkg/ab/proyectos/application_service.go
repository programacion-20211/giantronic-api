package proyectos

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerProyecto interface {
	CreateProyecto(idActividad int, valorEstandar int, valorCritico int, nroActividadesCronograma int, nroActividadesEjecutadas int, procentajeCumplimiento int, puntajeC int, puntajeD int, procentajeObtenido int) (*Proyecto, int, error)
	UpdateProyecto(id int, idActividad int, valorEstandar int, valorCritico int, nroActividadesCronograma int, nroActividadesEjecutadas int, procentajeCumplimiento int, puntajeC int, puntajeD int, procentajeObtenido int) (*Proyecto, int, error)
	DeleteProyecto(id int) (int, error)
	GetProyectoByID(id int) (*Proyecto, int, error)
	GetAllProyecto() ([]*Proyecto, error)
}

type service struct {
	repository ServicesProyectoRepository
	user       *models.User
	txID       string
}

func NewProyectoService(repository ServicesProyectoRepository, user *models.User, TxID string) PortsServerProyecto {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateProyecto(idActividad int, valorEstandar int, valorCritico int, nroActividadesCronograma int, nroActividadesEjecutadas int, procentajeCumplimiento int, puntajeC int, puntajeD int, procentajeObtenido int) (*Proyecto, int, error) {
	m := NewCreateProyecto(idActividad, valorEstandar, valorCritico, nroActividadesCronograma, nroActividadesEjecutadas, procentajeCumplimiento, puntajeC, puntajeD, procentajeObtenido)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create Proyecto :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateProyecto(id int, idActividad int, valorEstandar int, valorCritico int, nroActividadesCronograma int, nroActividadesEjecutadas int, procentajeCumplimiento int, puntajeC int, puntajeD int, procentajeObtenido int) (*Proyecto, int, error) {
	m := NewProyecto(id, idActividad, valorEstandar, valorCritico, nroActividadesCronograma, nroActividadesEjecutadas, procentajeCumplimiento, puntajeC, puntajeD, procentajeObtenido)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update Proyecto :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteProyecto(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetProyectoByID(id int) (*Proyecto, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllProyecto() ([]*Proyecto, error) {
	return s.repository.getAll()
}
