package proyectos

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// Proyecto  Model struct Proyecto
type Proyecto struct {
	ID                       int       `json:"id" db:"id" valid:"-"`
	IdActividad              int       `json:"id_actividad" db:"id_actividad" valid:"required"`
	ValorEstandar            int       `json:"valor_estandar" db:"valor_estandar" valid:"required"`
	ValorCritico             int       `json:"valor_critico" db:"valor_critico" valid:"required"`
	NroActividadesCronograma int       `json:"nro_actividades_cronograma" db:"nro_actividades_cronograma" valid:"required"`
	NroActividadesEjecutadas int       `json:"nro_actividades_ejecutadas" db:"nro_actividades_ejecutadas" valid:"required"`
	ProcentajeCumplimiento   int       `json:"procentaje_cumplimiento" db:"procentaje_cumplimiento" valid:"required"`
	PuntajeC                 int       `json:"puntaje_c" db:"puntaje_c" valid:"required"`
	PuntajeD                 int       `json:"puntaje_d" db:"puntaje_d" valid:"required"`
	ProcentajeObtenido       int       `json:"procentaje_obtenido" db:"procentaje_obtenido" valid:"required"`
	CreatedAt                time.Time `json:"created_at" db:"created_at"`
	UpdatedAt                time.Time `json:"updated_at" db:"updated_at"`
}

func NewProyecto(id int, idActividad int, valorEstandar int, valorCritico int, nroActividadesCronograma int, nroActividadesEjecutadas int, procentajeCumplimiento int, puntajeC int, puntajeD int, procentajeObtenido int) *Proyecto {
	return &Proyecto{
		ID:                       id,
		IdActividad:              idActividad,
		ValorEstandar:            valorEstandar,
		ValorCritico:             valorCritico,
		NroActividadesCronograma: nroActividadesCronograma,
		NroActividadesEjecutadas: nroActividadesEjecutadas,
		ProcentajeCumplimiento:   procentajeCumplimiento,
		PuntajeC:                 puntajeC,
		PuntajeD:                 puntajeD,
		ProcentajeObtenido:       procentajeObtenido,
	}
}

func NewCreateProyecto(idActividad int, valorEstandar int, valorCritico int, nroActividadesCronograma int, nroActividadesEjecutadas int, procentajeCumplimiento int, puntajeC int, puntajeD int, procentajeObtenido int) *Proyecto {
	return &Proyecto{
		IdActividad:              idActividad,
		ValorEstandar:            valorEstandar,
		ValorCritico:             valorCritico,
		NroActividadesCronograma: nroActividadesCronograma,
		NroActividadesEjecutadas: nroActividadesEjecutadas,
		ProcentajeCumplimiento:   procentajeCumplimiento,
		PuntajeC:                 puntajeC,
		PuntajeD:                 puntajeD,
		ProcentajeObtenido:       procentajeObtenido,
	}
}

func (m *Proyecto) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
