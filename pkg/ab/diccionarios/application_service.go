package diccionarios

import (
	"fmt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerDiccionario interface {
	CreateDiccionario(name string, value string, description string) (*Diccionario, int, error)
	UpdateDiccionario(id int, name string, value string, description string) (*Diccionario, int, error)
	DeleteDiccionario(id int) (int, error)
	GetDiccionarioByID(id int) (*Diccionario, int, error)
	GetAllDiccionario() ([]*Diccionario, error)
}

type service struct {
	repository ServicesDiccionarioRepository
	user       *models.User
	txID       string
}

func NewDiccionarioService(repository ServicesDiccionarioRepository, user *models.User, TxID string) PortsServerDiccionario {
	return &service{repository: repository, user: user, txID: TxID}
}

func (s *service) CreateDiccionario(name string, value string, description string) (*Diccionario, int, error) {
	m := NewCreateDiccionario(name, value, description)
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.create(m); err != nil {
		if err.Error() == "ecatch:108" {
			return m, 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't create Diccionario :", err)
		return m, 3, err
	}
	return m, 29, nil
}

func (s *service) UpdateDiccionario(id int, name string, value string, description string) (*Diccionario, int, error) {
	m := NewDiccionario(id, name, value, description)
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return m, 15, fmt.Errorf("id is required")
	}
	if valid, err := m.valid(); !valid {
		logger.Error.Println(s.txID, " - don't meet validations:", err)
		return m, 15, err
	}
	if err := s.repository.update(m); err != nil {
		logger.Error.Println(s.txID, " - couldn't update Diccionario :", err)
		return m, 18, err
	}
	return m, 29, nil
}

func (s *service) DeleteDiccionario(id int) (int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return 15, fmt.Errorf("id is required")
	}

	if err := s.repository.delete(id); err != nil {
		if err.Error() == "ecatch:108" {
			return 108, nil
		}
		logger.Error.Println(s.txID, " - couldn't update row:", err)
		return 20, err
	}
	return 28, nil
}

func (s *service) GetDiccionarioByID(id int) (*Diccionario, int, error) {
	if id == 0 {
		logger.Error.Println(s.txID, " - don't meet validations:", fmt.Errorf("id is required"))
		return nil, 15, fmt.Errorf("id is required")
	}
	m, err := s.repository.getByID(id)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn`t getByID row:", err)
		return nil, 22, err
	}
	return m, 29, nil
}

func (s *service) GetAllDiccionario() ([]*Diccionario, error) {
	return s.repository.getAll()
}
