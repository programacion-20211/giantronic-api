package diccionarios

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// Diccionario  Model struct Diccionario
type Diccionario struct {
	ID          int       `json:"id" db:"id" valid:"-"`
	Name        string    `json:"name" db:"name" valid:"required"`
	Value       string    `json:"value" db:"value" valid:"required"`
	Description string    `json:"description" db:"description" valid:"required"`
	CreatedAt   time.Time `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
}

func NewDiccionario(id int, name string, value string, description string) *Diccionario {
	return &Diccionario{
		ID:          id,
		Name:        name,
		Value:       value,
		Description: description,
	}
}

func NewCreateDiccionario(name string, value string, description string) *Diccionario {
	return &Diccionario{
		Name:        name,
		Value:       value,
		Description: description,
	}
}

func (m *Diccionario) valid() (bool, error) {
	result, err := govalidator.ValidateStruct(m)
	if err != nil {
		return result, err
	}
	return result, nil
}
