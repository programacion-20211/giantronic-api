package diccionarios

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesDiccionarioRepository interface {
	create(m *Diccionario) error
	update(m *Diccionario) error
	delete(id int) error
	getByID(id int) (*Diccionario, error)
	getAll() ([]*Diccionario, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesDiccionarioRepository {
	var s ServicesDiccionarioRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return newDiccionarioSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
