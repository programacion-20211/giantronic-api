package ab

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/bonificaciones"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/cargos"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/diccionarios"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/diponibilidad_servicio"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/empleados"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/evaluacion_actividades"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/mesa_ayuda"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/messages"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/niveles_servicio"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/periodos"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/productivity_indicators"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/proyectos"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/roles"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/usuarios"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab/usuarios_roles"
)

type Server struct {
	SrvBonificacion           bonificaciones.PortsServerBonificacion
	SrvCargos                 cargos.PortsServerCargo
	SrvDictionario            diccionarios.PortsServerDiccionario
	SrvDisponibilidadServicio diponibilidad_servicio.PortsServerDiponibilidadServicio
	SrvEmpleados              empleados.PortsServerEmpleado
	SrvEvaluacionActividades  evaluacion_actividades.PortsServerEvaluacionActividad
	SrvMesaAyuda              mesa_ayuda.PortsServerMesaAyuda
	SrvMessage                messages.PortsServerMessage
	SrvNivelesServicio        niveles_servicio.PortsServerNivelServicio
	SrvPeriodos               periodos.PortsServerPeriodo
	SrvProductivityIndicator  productivity_indicators.PortsServerProductivityIndicator
	SrvProyectos              proyectos.PortsServerProyecto
	SrvRoles                  roles.PortsServerRol
	SrvUsuario                usuarios.PortsServerUsuario
	SrvUsuarioRol             usuarios_roles.PortsServerUsuarioRol
}

func NewServerAB(db *sqlx.DB, user *models.User, txID string) *Server {
	repoBonificacion := bonificaciones.FactoryStorage(db, user, txID)
	srvBonificacion := bonificaciones.NewBonificacionService(repoBonificacion, user, txID)

	repoCargo := cargos.FactoryStorage(db, user, txID)
	srvCargos := cargos.NewCargoService(repoCargo, user, txID)

	repoDictionario := diccionarios.FactoryStorage(db, user, txID)
	srvDictionario := diccionarios.NewDiccionarioService(repoDictionario, user, txID)

	repoDisponibilidadServicio := diponibilidad_servicio.FactoryStorage(db, user, txID)
	srvDisponibilidadServicio := diponibilidad_servicio.NewDiponibilidadServicioService(repoDisponibilidadServicio, user, txID)

	repoEmpleados := empleados.FactoryStorage(db, user, txID)
	srvEmpleados := empleados.NewEmpleadoService(repoEmpleados, user, txID)

	repoEvaluacionActividades := evaluacion_actividades.FactoryStorage(db, user, txID)
	srvEvaluacionActividades := evaluacion_actividades.NewEvaluacionActividadService(repoEvaluacionActividades, user, txID)

	repoMesaAyuda := mesa_ayuda.FactoryStorage(db, user, txID)
	srvMesaAyuda := mesa_ayuda.NewMesaAyudaService(repoMesaAyuda, user, txID)

	repoMessages := messages.FactoryStorage(db, user, txID)
	srvMessages := messages.NewMessageService(repoMessages, user, txID)

	repoNivelesServicio := niveles_servicio.FactoryStorage(db, user, txID)
	srvNivelesServicio := niveles_servicio.NewNivelServicioService(repoNivelesServicio, user, txID)

	repoPeriodos := periodos.FactoryStorage(db, user, txID)
	srvPeriodos := periodos.NewPeriodoService(repoPeriodos, user, txID)

	repoProductivityIndicators := productivity_indicators.FactoryStorage(db, user, txID)
	srvProductivityIndicator := productivity_indicators.NewProductivityIndicatorService(repoProductivityIndicators, user, txID)

	repoProyectos := proyectos.FactoryStorage(db, user, txID)
	srvProyectos := proyectos.NewProyectoService(repoProyectos, user, txID)

	repoRoles := roles.FactoryStorage(db, user, txID)
	srvRoles := roles.NewRolService(repoRoles, user, txID)

	repoUsuarios := usuarios.FactoryStorage(db, user, txID)
	srvUsuario := usuarios.NewUsuarioService(repoUsuarios, user, txID)

	repoUsuariosRoles := usuarios_roles.FactoryStorage(db, user, txID)
	srvUsuarioRol := usuarios_roles.NewUsuarioRolService(repoUsuariosRoles, user, txID)

	return &Server{

		SrvBonificacion:           srvBonificacion,
		SrvCargos:                 srvCargos,
		SrvDictionario:            srvDictionario,
		SrvDisponibilidadServicio: srvDisponibilidadServicio,
		SrvEmpleados:              srvEmpleados,
		SrvEvaluacionActividades:  srvEvaluacionActividades,
		SrvMesaAyuda:              srvMesaAyuda,
		SrvMessage:                srvMessages,
		SrvNivelesServicio:        srvNivelesServicio,
		SrvPeriodos:               srvPeriodos,
		SrvProductivityIndicator:  srvProductivityIndicator,
		SrvProyectos:              srvProyectos,
		SrvRoles:                  srvRoles,
		SrvUsuario:                srvUsuario,
		SrvUsuarioRol:             srvUsuarioRol,
	}
}
