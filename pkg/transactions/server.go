package transactions

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/transactions/report"
)

type Server struct {
	SrvReport report.PortsServerReport
}

func NewServerReport(db *sqlx.DB, user *models.User, txID string) *Server {

	repoReport := report.FactoryStorage(db, user, txID)
	srvReport := report.NewReportService(repoReport, user, txID)

	return &Server{

		SrvReport: srvReport,
	}
}
