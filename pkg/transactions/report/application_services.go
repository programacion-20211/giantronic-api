package report

import (
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type PortsServerReport interface {
	ExecuteReport(procedure string, parameters map[string]interface{}, option int) ([]map[string]interface{}, error)
}

type Service struct {
	repository ServicesReportRepository
	user       *models.User
	txID       string
}

func NewReportService(repository ServicesReportRepository, user *models.User, TxID string) PortsServerReport {
	return Service{repository: repository, user: user, txID: TxID}
}

func (s Service) ExecuteReport(procedure string, parameters map[string]interface{}, option int) ([]map[string]interface{}, error) {
	rs, err := s.repository.ExecuteSP(procedure, parameters, option)
	if err != nil {
		logger.Error.Println(s.txID, " - couldn't execute Report :", err)
		return nil, err
	}
	return rs, nil
}
