package report

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

const (
	SqlServer = "sqlserver"
)

type ServicesReportRepository interface {
	ExecuteSP(procedure string, parameters map[string]interface{}, option int) ([]map[string]interface{}, error)
}

func FactoryStorage(db *sqlx.DB, user *models.User, txID string) ServicesReportRepository {
	var s ServicesReportRepository
	engine := db.DriverName()
	switch engine {
	case SqlServer:
		return NewReportSqlServerRepository(db, user, txID)
	default:
		logger.Error.Println("el motor de base de datos no está implementado.", engine)
	}
	return s
}
