package login

import (
	"net/http"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"

	jwt_login "gitlab.com/laboratorios-programacion/giantronic-api/internal/jwt"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/password"

	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/msg"
)

type handlerLogin struct {
	DB   *sqlx.DB
	TxID string
}

// Login godoc
// @Summary blockchain
// @Description GetBlockToMine
// @Accept  json
// @Produce  json
// @Success 200 {object} responseBlockToMine
// @Success 202 {object} dataBlockToMine
// @Router /api/v1/block-to-mine [get]
// @Authorization Bearer token
func (h *handlerLogin) Login(c *fiber.Ctx) error {

	res := models.NewResponse(true)
	m := requestLogin{}
	err := c.BodyParser(&m)
	if err != nil {
		logger.Error.Printf("couldn't bind model login: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	usr, cod, err := srv.SrvUsuario.GetUsuarioByNick(m.Email)
	if err != nil {
		logger.Warning.Printf("couldn't login: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(cod, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	if usr == nil {
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(70, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	if !password.Compare(usr.DNI, usr.Contrasena, m.Password) {
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(10, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	token, cod, err := jwt_login.GenerateJWT(srv.SrvUsuario.ResetFieldsUser(usr))
	if err != nil {
		logger.Error.Printf("couldn't get token:%s - %v", h.TxID, err)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	authRes := Token{
		AccessToken: token,
		DNI:         usr.DNI,
		Correo:      usr.Correo,
		Nombre:      usr.Nombre,
		Apellidop:   usr.Apellidop,
		Apellidom:   usr.Apellidom,
		Telefono:    usr.Telefono,
		Direccion:   usr.Direccion,
		Nick:        usr.Nick,
		Perfil:      usr.Perfil,
	}
	res.Data = authRes
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
