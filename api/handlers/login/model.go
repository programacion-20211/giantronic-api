package login

type requestLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Token struct {
	AccessToken string `json:"access_token"`
	DNI         string `json:"dni" db:"dni" valid:"-"`
	Correo      string `json:"correo" db:"correo" valid:"-"`
	Nombre      string `json:"nombre" db:"nombre" valid:"required"`
	Apellidop   string `json:"apellidop" db:"apellidop" valid:"required"`
	Apellidom   string `json:"apellidom" db:"apellidom" valid:"required"`
	Telefono    string `json:"telefono" db:"telefono" valid:"required"`
	Direccion   string `json:"direccion" db:"direccion" valid:"required"`
	Nick        string `json:"nick" db:"nick" valid:"required"`
	Perfil      string `json:"perfil" db:"perfil" valid:"required"`
}
