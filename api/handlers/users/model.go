package users

type requestCreateUser struct {
	DNI        string `json:"dni" db:"dni" valid:"-"`
	Correo     string `json:"correo" db:"correo" valid:"-"`
	Nombre     string `json:"nombre" db:"nombre" valid:"required"`
	Apellidop  string `json:"apellidop" db:"apellidop" valid:"required"`
	Apellidom  string `json:"apellidom" db:"apellidom" valid:"required"`
	Telefono   string `json:"telefono" db:"telefono" valid:"required"`
	Direccion  string `json:"direccion" db:"direccion" valid:"required"`
	Nick       string `json:"nick" db:"nick" valid:"required"`
	Contrasena string `json:"contraseña" db:"contraseña" valid:"required"`
	Perfil     string `json:"perfil" db:"perfil" valid:"required"`
}
type requestUpdatePassword struct {
	DNI        string `json:"dni" db:"dni" valid:"-"`
	Contrasena string `json:"contraseña" db:"contraseña" valid:"required"`
}
