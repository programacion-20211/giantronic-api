package users

import (
	"net/http"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/password"

	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/msg"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
)

type handlerUser struct {
	DB   *sqlx.DB
	TxID string
}

// create user godoc
// @Summary blockchain
// @Description Create User
// @Accept  json
// @Produce  json
// @Success 200 {object} requestCreateUser
// @Success 202 {object} responseCreateUser
// @Router /api/v1/user/create [post]
func (h *handlerUser) createUser(c *fiber.Ctx) error {
	res := models.NewResponse(true)
	m := requestCreateUser{}
	err := c.BodyParser(&m)
	if err != nil {
		logger.Error.Printf("couldn't bind model create wallets: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	m.Contrasena = password.Encrypt(m.Contrasena)
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	usr, code, err := srv.SrvUsuario.CreateUsuario(m.DNI, m.Correo, m.Nombre, m.Apellidop, m.Apellidom, m.Telefono, m.Direccion, m.Nick, m.Contrasena, m.Perfil)
	if err != nil {
		logger.Error.Printf("couldn't create User: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(code, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	usr.Contrasena = ""

	res.Data = usr
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}

// edit user godoc
// @Summary blockchain
// @Description Edit User
// @Accept  json
// @Produce  json
// @Success 200 {object} requestCreateUser
// @Success 202 {object} responseCreateUser
// @Router /api/v1/user/create [put]
func (h *handlerUser) editUser(c *fiber.Ctx) error {
	res := models.NewResponse(true)
	m := requestCreateUser{}
	err := c.BodyParser(&m)
	if err != nil {
		logger.Error.Printf("couldn't bind model create wallets: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	m.Contrasena = password.Encrypt(m.Contrasena)
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	usr, code, err := srv.SrvUsuario.UpdateUsuario(m.DNI, m.Correo, m.Nombre, m.Apellidop, m.Apellidom, m.Telefono, m.Direccion, m.Nick, m.Contrasena, m.Perfil)
	if err != nil {
		logger.Error.Printf("couldn't update User: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(code, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	usr.Contrasena = ""

	res.Data = usr
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}

func (h *handlerUser) updatePassword(c *fiber.Ctx) error {
	res := models.NewResponse(true)
	m := requestUpdatePassword{}
	err := c.BodyParser(&m)
	if err != nil {
		logger.Error.Printf("couldn't bind model create wallets: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	m.Contrasena = password.Encrypt(m.Contrasena)
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	code, err := srv.SrvUsuario.UpdatePassword(m.DNI, m.Contrasena)
	if err != nil {
		logger.Error.Printf("couldn't update User: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(code, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = true
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
