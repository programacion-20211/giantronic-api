package evaluacion_actividades

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/middleware"
)

func RouterEvaluacionActividades(app *fiber.App, db *sqlx.DB, txID string) {
	h := handlerEvaluacion{DB: db, TxID: txID}
	api := app.Group("/api")
	v1 := api.Group("/v1")
	v1.Post("/evaluacion", middleware.JWTProtected(), h.CreateEvaluacion)
	v1.Get("/evaluacion/periodo/:id_periodo", middleware.JWTProtected(), h.GetEvaluacionPorPeriodo)

}
