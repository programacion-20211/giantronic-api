package evaluacion_actividades

type requestCreateEvaluacion struct {
	IdEmpleado int `json:"id_empleado"`
	IdPeriodo  int `json:"id_periodo"`
}
