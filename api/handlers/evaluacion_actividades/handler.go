package evaluacion_actividades

import (
	"net/http"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/msg"
	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab"
)

type handlerEvaluacion struct {
	DB   *sqlx.DB
	TxID string
}

func (h *handlerEvaluacion) CreateEvaluacion(c *fiber.Ctx) error {
	res := models.NewResponse(true)
	m := requestCreateEvaluacion{}
	err := c.BodyParser(&m)
	if err != nil {
		logger.Error.Printf("couldn't bind model CreateEvaluacion: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	EvalActivities, cod, err := srv.SrvEvaluacionActividades.CreateEvaluacionActividad(m.IdEmpleado, m.IdPeriodo)
	if err != nil {
		logger.Warning.Printf("couldn't create CreateEvaluacion: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(cod, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = EvalActivities
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}

func (h *handlerEvaluacion) GetEvaluacionPorPeriodo(c *fiber.Ctx) error {
	res := models.NewResponse(true)
	periodo, err := strconv.Atoi(c.Params(":id_periodo"))
	if err != nil {
		logger.Error.Printf("couldn't bind periodo en get Evaluacion por periodo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	EvalActivities, code, err := srv.SrvEvaluacionActividades.GetAllEvaluacionByPeriodo(periodo)
	if err != nil {
		logger.Warning.Printf("couldn't create CreateEvaluacion: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(code, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = EvalActivities
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
