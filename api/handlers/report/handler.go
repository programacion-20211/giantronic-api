package report

import (
	"net/http"

	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/transactions"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"
)

type handlerReport struct {
	dB   *sqlx.DB
	user *models.User
	txID string
}

// ExecuteReportProcedure godoc
// @Summary Result Admin Operation
// @Description Result Admin Operation
// @Accept  json
// @Produce  json
// @Success 200 {object} ResponseReport
// @Security ApiKeyAuth
// @Router /api/v1/report/data/info [get]
func (h *handlerReport) ExecuteReportProcedure(c *fiber.Ctx) error {
	res := ResponseReport{Error: true}
	rq := RequestReport{}
	err := c.BodyParser(&rq)
	if err != nil {
		logger.Error.Printf("couldn't bind model RequestProcess: %v", err)
		res.Code = 1
		res.Type = "Error"
		res.Msg = "couldn't bind model RequestProcess"
		return c.Status(http.StatusAccepted).JSON(res)
	}

	srvE11 := transactions.NewServerReport(h.dB, h.user, h.txID)
	resReport, err := srvE11.SrvReport.ExecuteReport(rq.Procedure, rq.Parameters, 0)
	if err != nil {
		logger.Error.Printf("Couldn't execute report: %v", err)
		res.Code = 1
		res.Type = "Error"
		res.Msg = "Couldn't execute report"
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = resReport
	res.Error = false
	res.Code = 29
	res.Type = "Success"
	res.Msg = "Procesado Correctamente"
	return c.Status(http.StatusOK).JSON(res)
}
