package report

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
)

func RouterProcess(app *fiber.App, db *sqlx.DB, txID string) {
	h := handlerReport{dB: db, txID: txID}
	api := app.Group("/api")
	v1 := api.Group("/v1")
	process := v1.Group("/report")
	process.Post("", h.ExecuteReportProcedure)
}
