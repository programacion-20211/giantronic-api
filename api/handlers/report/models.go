package report

type ResponseReport struct {
	Error bool        `json:"error"`
	Data  interface{} `json:"data"`
	Code  int         `json:"code"`
	Type  string      `json:"type"`
	Msg   string      `json:"msg"`
}

type RequestReport struct {
	Procedure  string                 `json:"procedure"`
	Parameters map[string]interface{} `json:"parameters"`
}
