package cargos

type requestCargo struct {
	Id                               int    `json:"id"`
	Cargo                            string `json:"cargo"`
	PorcetajeCumplimientoActividades int    `json:"porcetaje_cumplimiento_actividades"`
	PorcetajeMesaAyuda               int    `json:"porcetaje_mesa_ayuda"`
	PorcetajeDisponibilidadServicio  int    `json:"porcetaje_disponibilidad_servicio"`
	PorcetajeNivelesServicio         int    `json:"porcetaje_niveles_servicio"`
}
