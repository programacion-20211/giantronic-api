package cargos

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/middleware"
)

func RouterCargo(app *fiber.App, db *sqlx.DB, txID string) {
	h := handlerCargo{DB: db, TxID: txID}
	api := app.Group("/api")
	v1 := api.Group("/v1")
	v1.Post("/cargo", middleware.JWTProtected(), h.CreateCargo)
	v1.Put("/cargo", middleware.JWTProtected(), h.UpdateCargo)
	v1.Delete("/cargo/:id_cargo", middleware.JWTProtected(), h.DeleteCargo)
	v1.Get("/cargo", middleware.JWTProtected(), h.GetCargos)
	v1.Get("/cargo/:id_cargo", middleware.JWTProtected(), h.GetCargosByID)
}
