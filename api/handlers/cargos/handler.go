package cargos

import (
	"net/http"
	"strconv"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"

	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/msg"
)

type handlerCargo struct {
	DB   *sqlx.DB
	TxID string
}

func (h *handlerCargo) CreateCargo(c *fiber.Ctx) error {

	res := models.NewResponse(true)
	m := requestCargo{}
	err := c.BodyParser(&m)
	if err != nil {
		logger.Error.Printf("couldn't bind model cargo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	cargo, cod, err := srv.SrvCargos.CreateCargo(m.Cargo, m.PorcetajeCumplimientoActividades, m.PorcetajeMesaAyuda,
		m.PorcetajeDisponibilidadServicio, m.PorcetajeNivelesServicio)
	if err != nil {
		logger.Warning.Printf("couldn't create cargo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(cod, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = cargo
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
func (h *handlerCargo) UpdateCargo(c *fiber.Ctx) error {

	res := models.NewResponse(true)
	m := requestCargo{}
	err := c.BodyParser(&m)
	if err != nil {
		logger.Error.Printf("couldn't bind model cargo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	cargo, cod, err := srv.SrvCargos.UpdateCargo(m.Id, m.Cargo, m.PorcetajeCumplimientoActividades, m.PorcetajeMesaAyuda,
		m.PorcetajeDisponibilidadServicio, m.PorcetajeNivelesServicio)
	if err != nil {
		logger.Warning.Printf("couldn't create cargo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(cod, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = cargo
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
func (h *handlerCargo) DeleteCargo(c *fiber.Ctx) error {

	res := models.NewResponse(true)
	id, err := strconv.Atoi(c.Params("id_cargo"))
	if err != nil {
		logger.Error.Printf("couldn't bind model cargo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	cargo, err := srv.SrvCargos.DeleteCargo(id)
	if err != nil {
		logger.Warning.Printf("couldn't create cargo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(20, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = cargo
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
func (h *handlerCargo) GetCargos(c *fiber.Ctx) error {
	res := models.NewResponse(true)

	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	cargos, err := srv.SrvCargos.GetAllCargo()
	if err != nil {
		logger.Warning.Printf("couldn't get all cargo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(22, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = cargos
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
func (h *handlerCargo) GetCargosByID(c *fiber.Ctx) error {
	res := models.NewResponse(true)

	id, err := strconv.Atoi(c.Params("id_cargo"))
	if err != nil {
		logger.Error.Printf("couldn't bind model cargo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	cargos, code, err := srv.SrvCargos.GetCargoByID(id)
	if err != nil {
		logger.Warning.Printf("couldn't get all cargo: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(code, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = cargos
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
