package productivity_indicators

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/middleware"
)

func RouterhProductivityIndicators(app *fiber.App, db *sqlx.DB, txID string) {
	h := handlerProductivityIndicators{DB: db, TxID: txID}
	api := app.Group("/api")
	v1 := api.Group("/v1")
	v1.Post("/productivity/indicators", middleware.JWTProtected(), h.GetProductivityIndicators)
}
