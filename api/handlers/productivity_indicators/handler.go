package productivity_indicators

import (
	"net/http"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"

	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/msg"
)

type handlerProductivityIndicators struct {
	DB   *sqlx.DB
	TxID string
}

func (h *handlerProductivityIndicators) GetProductivityIndicators(c *fiber.Ctx) error {

	res := models.NewResponse(true)
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	prodIndicators, err := srv.SrvProductivityIndicator.GetAllProductivityIndicator()
	if err != nil {
		logger.Warning.Printf("couldn't create empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(21, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = prodIndicators
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
