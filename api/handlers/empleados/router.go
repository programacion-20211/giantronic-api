package empleados

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/middleware"
)

func RouterEmpleados(app *fiber.App, db *sqlx.DB, txID string) {
	h := handlerEmpleado{DB: db, TxID: txID}
	api := app.Group("/api")
	v1 := api.Group("/v1")
	v1.Post("/empleado", middleware.JWTProtected(), h.CreateEmpleado)
	v1.Put("/empleado", middleware.JWTProtected(), h.UpdateEmpleado)
	v1.Delete("/empleado/:id_empleado", middleware.JWTProtected(), h.DeteleEmpleado)
	v1.Get("/empleado", middleware.JWTProtected(), h.GetEmpleados)
	v1.Get("/empleado/:id_empleado", middleware.JWTProtected(), h.GetEmpleadoById)
}
