package empleados

type requestEmpleado struct {
	Id                   int    `json:"id"`
	TipoIdentificacion   int    `json:"tipo_identificacion"`
	NumeroIdentificacion string `json:"numero_identificacion"`
	Nombre               string `json:"nombre"`
	IdCargo              int    `json:"id_cargo"`
}
