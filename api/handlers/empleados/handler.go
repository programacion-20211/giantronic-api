package empleados

import (
	"net/http"
	"strconv"

	"gitlab.com/laboratorios-programacion/giantronic-api/internal/models"

	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/logger"
	"gitlab.com/laboratorios-programacion/giantronic-api/internal/msg"
)

type handlerEmpleado struct {
	DB   *sqlx.DB
	TxID string
}

func (h *handlerEmpleado) CreateEmpleado(c *fiber.Ctx) error {

	res := models.NewResponse(true)
	m := requestEmpleado{}
	err := c.BodyParser(&m)
	if err != nil {
		logger.Error.Printf("couldn't bind model empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	empleado, cod, err := srv.SrvEmpleados.CreateEmpleado(m.TipoIdentificacion, m.NumeroIdentificacion, m.Nombre, m.IdCargo)
	if err != nil {
		logger.Warning.Printf("couldn't create empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(cod, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = empleado
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}

func (h *handlerEmpleado) UpdateEmpleado(c *fiber.Ctx) error {

	res := models.NewResponse(true)
	m := requestEmpleado{}
	err := c.BodyParser(&m)
	if err != nil {
		logger.Error.Printf("couldn't bind model empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	empleado, cod, err := srv.SrvEmpleados.UpdateEmpleado(m.Id, m.TipoIdentificacion, m.NumeroIdentificacion, m.Nombre, m.IdCargo)
	if err != nil {
		logger.Warning.Printf("couldn't update empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(cod, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = empleado
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}

func (h *handlerEmpleado) DeteleEmpleado(c *fiber.Ctx) error {

	res := models.NewResponse(true)
	id, err := strconv.Atoi(c.Params("id_empleado"))
	if err != nil {
		logger.Error.Printf("couldn't bind id empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}
	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	empleado, err := srv.SrvEmpleados.DeleteEmpleado(id)
	if err != nil {
		logger.Warning.Printf("couldn't create empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(20, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = empleado
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}

func (h *handlerEmpleado) GetEmpleados(c *fiber.Ctx) error {

	res := models.NewResponse(true)

	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	empleados, err := srv.SrvEmpleados.GetAllEmpleado()
	if err != nil {
		logger.Warning.Printf("couldn't create empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(22, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = empleados
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}

func (h *handlerEmpleado) GetEmpleadoById(c *fiber.Ctx) error {

	res := models.NewResponse(true)

	srv := ab.NewServerAB(h.DB, nil, h.TxID)
	id, err := strconv.Atoi(c.Params("id_empleado"))
	if err != nil {
		logger.Error.Printf("couldn't bind id empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(1, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	empleados, code, err := srv.SrvEmpleados.GetEmpleadoByID(id)
	if err != nil {
		logger.Warning.Printf("couldn't create empleado: %v", err)
		res.Code, res.TypeMsg, res.Msg = msg.GetByCode(code, h.DB, h.TxID)
		return c.Status(http.StatusAccepted).JSON(res)
	}

	res.Data = empleados
	res.Code, res.TypeMsg, res.Msg = msg.GetByCode(29, h.DB, h.TxID)
	res.Error = false
	return c.Status(http.StatusOK).JSON(res)
}
