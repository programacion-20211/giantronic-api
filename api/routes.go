package api

import (
	"github.com/ansrivas/fiberprometheus/v2"
	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/laboratorios-programacion/giantronic-api/api/handlers/cargos"
	"gitlab.com/laboratorios-programacion/giantronic-api/api/handlers/empleados"
	"gitlab.com/laboratorios-programacion/giantronic-api/api/handlers/login"
	"gitlab.com/laboratorios-programacion/giantronic-api/api/handlers/productivity_indicators"
	"gitlab.com/laboratorios-programacion/giantronic-api/api/handlers/report"
	"gitlab.com/laboratorios-programacion/giantronic-api/api/handlers/users"
)

// @title bjungle Blockchain API.
// @version 1.0
// @description Documentation Api Blockchain.
// @termsOfService https://www.bjungle-id.com/terms/
// @contact.name API Support.
// @contact.email info@bjungle-id.com
// @license.name Software Owner
// @license.url https://www.bjungle-id.com/licenses/LICENSE-1.0.html
// @host localhost:50025
// @BasePath /
func routes(db *sqlx.DB, loggerHttp bool, allowedOrigins string) *fiber.App {
	app := fiber.New()

	prometheus := fiberprometheus.New("nexumApiAuth")
	prometheus.RegisterAt(app, "/metrics")

	//app.Get("/swagger/*", fiberSwagger.Handler)
	app.Get("/swagger/*", swagger.New(swagger.Config{ // custom
		URL:         "/swagger/doc.json",
		DeepLinking: false,
	}))

	app.Static("/", "./img")
	app.Static("/giantrionic_website/", "./giantrionic_website")
	app.Use(recover.New())
	app.Use(prometheus.Middleware)
	app.Use(cors.New(cors.Config{
		AllowOrigins: allowedOrigins,
		AllowHeaders: "Origin, X-Requested-With, Content-Type, Accept, Authorization",
		AllowMethods: "GET,POST,DELETE,PUT",
	}))
	if loggerHttp {
		app.Use(logger.New())
	}
	TxID := uuid.New().String()

	loadRoutes(app, db, TxID)

	return app
}

func loadRoutes(app *fiber.App, db *sqlx.DB, TxID string) {
	users.RouterCreateUser(app, db, TxID)
	login.RouterLogin(app, db, TxID)
	empleados.RouterEmpleados(app, db, TxID)
	cargos.RouterCargo(app, db, TxID)
	productivity_indicators.RouterhProductivityIndicators(app, db, TxID)
	report.RouterProcess(app, db, TxID)
}
