package models

type Response struct {
	Error   bool        `json:"error"`
	Data    interface{} `json:"data"`
	Code    int         `json:"code"`
	TypeMsg int         `json:"type"`
	Msg     string      `json:"msg"`
}

func NewResponse(error bool) Response {
	return Response{
		Error: error,
	}
}
