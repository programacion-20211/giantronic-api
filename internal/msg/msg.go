package msg

import (
	"strconv"

	"gitlab.com/laboratorios-programacion/giantronic-api/pkg/ab"

	"github.com/jmoiron/sqlx"
)

func GetByCode(code int, db *sqlx.DB, txID string) (int, int, string) {
	codRes := 0
	msg := ""
	srv := ab.NewServerAB(db, nil, txID)
	m, codErr, err := srv.SrvMessage.GetMessageByID(code)
	if err != nil {
		return codRes, 0, strconv.Itoa(codErr)
	}

	codRes = m.ID
	msg = m.Spa
	return codRes, m.TypeMessage, msg
}
